Package for Multi-Criteria Decision Analysis named `mcda` (**Alpha**).


# Package description

This package is used as a basis to represent MCDA problems as well as solve them.
It currently contains functionalities to accurately describe a MCDA problem.
It also contains relatively low-level plotting functions to visualize a MCDA problem and its solution.


# Requirements

To be able to plot relations and outranking graphs, this package requires that `graphviz` be installed.
On Debian/Ubuntu this is done with:

```bash
sudo apt-get install graphviz
```


# Documentation

Documentation on this package can be found [here](https://py-mcda.readthedocs.io/).


# Developers' corner

This package is [PEP8](https://www.python.org/dev/peps/pep-0008/) compliant with a maximum line length of **79**, and is statically typed using [type hints](https://docs.python.org/3/library/typing.html).


# Known issues

None
