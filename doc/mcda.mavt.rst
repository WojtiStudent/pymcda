mcda.mavt package
=================

.. automodule:: mcda.mavt
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

mcda.mavt.aggregators module
----------------------------

.. automodule:: mcda.mavt.aggregators
   :members:
   :undoc-members:
   :show-inheritance:

mcda.mavt.uta module
--------------------

.. automodule:: mcda.mavt.uta
   :members:
   :undoc-members:
   :show-inheritance:
