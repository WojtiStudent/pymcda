.. Py-MCDA documentation master file, created by
   sphinx-quickstart on Wed Aug  4 16:03:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Py-MCDA's documentation!
===================================

This package is about Multi-Criteria Decision Analyzis.

It is released on PyPI as `mcda <https://pypi.org/project/mcda/>`_.

It is an open source project hosted on `gitlab <https://gitlab.com/decide.imt-atlantique/pymcda>`_.


Requirements
============

To be able to plot relations and outranking graphs, this package requires that `graphviz` be installed.
On Debian/Ubuntu this is done with:

.. code-block:: console

   $ sudo apt-get install graphviz


Installation
============

If you want to simply use this package, simply install it from `PyPI <https://pypi.org/project/mcda/>`_:

.. code-block:: console

   $ pip install mcda

If you want to contribute to this package development, we recommend you to visit the
`gitlab repository <https://gitlab.com/decide.imt-atlantique/pymcda>`_ and read the
`installation instructions <https://gitlab.com/decide.imt-atlantique/pymcda/-/blob/dev/INSTALL.md>`_ there.


API
===

.. toctree::
   :maxdepth: 4

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


References
==========

.. bibliography::
