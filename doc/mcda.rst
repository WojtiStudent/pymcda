mcda package
============

.. automodule:: mcda
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   mcda.core
   mcda.dea
   mcda.mavt
   mcda.outranking
   mcda.plot

Submodules
----------

mcda.utils module
-----------------

.. automodule:: mcda.utils
   :members:
   :undoc-members:
   :show-inheritance:
