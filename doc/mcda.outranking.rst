mcda.outranking package
=======================

.. automodule:: mcda.outranking
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

mcda.outranking.electre1 module
-------------------------------

.. automodule:: mcda.outranking.electre1
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.electre2 module
-------------------------------

.. automodule:: mcda.outranking.electre2
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.electre3 module
-------------------------------

.. automodule:: mcda.outranking.electre3
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.electre\_tri module
-----------------------------------

.. automodule:: mcda.outranking.electre_tri
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.promethee1 module
---------------------------------

.. automodule:: mcda.outranking.promethee1
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.promethee2 module
---------------------------------

.. automodule:: mcda.outranking.promethee2
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.prometheeGAIA module
------------------------------------

.. automodule:: mcda.outranking.prometheeGAIA
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.promethee\_common module
----------------------------------------

.. automodule:: mcda.outranking.promethee_common
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.srmp module
---------------------------

.. automodule:: mcda.outranking.srmp
   :members:
   :undoc-members:
   :show-inheritance:

mcda.outranking.utils module
----------------------------

.. automodule:: mcda.outranking.utils
   :members:
   :undoc-members:
   :show-inheritance:
