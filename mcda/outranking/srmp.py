"""This module implements the SRMP algorithm,
as well as the preference elicitation algorithm and plot functions.

Implementation and naming conventions are taken from
:cite:p:`olteanu2022preference`.
"""
from itertools import permutations
from typing import Dict, List, Tuple, cast

import numpy as np
from pandas import Series, concat
from pulp import (
    LpBinary,
    LpContinuous,
    LpMaximize,
    LpProblem,
    LpVariable,
    lpSum,
)
from pulp import value as pulp_value
from typing_extensions import TypedDict

from ..core.aliases import Function, NumericValue, PerformanceTable, Value
from ..core.performance_table import (
    apply_criteria_functions,
    apply_criteria_weights,
    normalize,
    normalize_without_scales,
    transform,
)
from ..core.relations import Relation, RelationType
from ..core.scales import (
    PreferenceDirection,
    QualitativeScale,
    QuantitativeScale,
    Scale,
    get_normalized_scale,
    is_better_or_equal,
)
from ..plot.plot import (
    Annotation,
    AreaPlot,
    Axis,
    BarPlot,
    Figure,
    HorizontalStripes,
    LinePlot,
    ParallelCoordinatesPlot,
    StackedBarPlot,
    Text,
)


def preference_relation(
    profile: Series,
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    criteria_weights: Dict[str, NumericValue],
    **kwargs,
) -> PerformanceTable:
    """Define a preference relation related to a profile

    :param profile:
    :param performance_table:
    :param scales:
    :param criteria_weights:
    :return: preference matrix
    """
    functions = {
        criterion: (
            cast(
                Function,
                lambda x, c=criterion: is_better_or_equal(
                    x, profile[c], scales[c]
                ),
            )
        )  # https://bugs.python.org/issue13652
        for criterion in scales.keys()
    }

    conditional_weighted_sum = apply_criteria_weights(
        apply_criteria_functions(performance_table, functions),
        criteria_weights,
    ).sum(1)

    return PerformanceTable(
        [
            [
                conditional_weighted_sum[ai] >= conditional_weighted_sum[aj]
                for aj in performance_table.index
            ]
            for ai in performance_table.index
        ],
        index=performance_table.index,
        columns=performance_table.index,
        dtype="int64",
    )


def ranking(
    relations: List[PerformanceTable],
    lexicographic_order: List[int],
    **kwargs,
) -> Series:
    """Rank alternatives

    :param relations: list of preference relations
    :param lexicographic_order: profile indices used sequentially to rank
    :return: the outranking total order (higher number to better alternative)
    """
    relations_ordered = [relations[i] for i in lexicographic_order]
    n = len(relations_ordered)
    score = sum(
        [
            PerformanceTable(2 ** (n - 1 - i) * relations_ordered[i])
            for i in range(n)
        ],
        PerformanceTable(
            0,
            index=relations_ordered[0].index,
            columns=relations_ordered[0].columns,
        ),
    )
    outranking_matrix = score - score.transpose() >= 0
    return outranking_matrix.sum(1)


def srmp(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    criteria_weights: Dict[str, NumericValue],
    profiles: PerformanceTable,
    lexicographic_order: List[int],
    **kwargs,
) -> Series:
    """Compute the SRMP algorithm

    :param performance_table:
    :param scales:
    :param criteria_weights:
    :param profiles:
    :param lexicographic_order: profile indices used sequentially to rank
    :return: the outranking total order (higher number to better alternative)
    """
    relations = profiles.apply(
        preference_relation,
        1,
        performance_table=performance_table,
        scales=scales,
        criteria_weights=criteria_weights,
    ).tolist()
    return ranking(relations, lexicographic_order)


##########################
# Preference Elicitation #
##########################


class PreferenceElicitationResult(TypedDict):
    criteria_weights: Dict[str, NumericValue]
    profiles: PerformanceTable
    lexicographic_order: List[int]
    fitness: NumericValue


def normalized_preference_elicitation(
    performance_table: PerformanceTable,
    relations: List[Relation],
    lexicographic_order: List[int],
    inconsistencies: bool = True,
    gamma: NumericValue = 0.001,
    non_dictator: bool = False,
    solver_args: dict = None,
) -> PreferenceElicitationResult:
    """Compute the preference elicitation algorithm with normalized performances

    :param performance_table:
    :param relations: pairwise relations between alternatives
    :param lexicographic_order: profile indices used sequentially to rank
    :param inconsistencies:
        if ``True`` inconsistent comparisons will be taken into account
    :param gamma: value used for modeling strict inequalities
    :param non_dictator: if ``True`` prevent dictator weights (> 0.5)
    :param solver_args: extra arguments supplied to the solver
    :return: the inferred criteria weights, profiles, lexicographic order
        and the proportion of fulfilled comparisons

    .. note::
        Reference profiles returned are in normalized scales
    """
    solver_args = {} if solver_args is None else solver_args

    ##############
    # Parameters #
    ##############

    # List of alternatives
    A_star = set()
    for a, b, _ in relations:
        A_star.add(a)
        A_star.add(b)
    # List of criteria
    M = performance_table.columns
    # Number of profiles
    k = len(lexicographic_order)
    # Indices of profiles
    profile_indices = list(range(1, k + 1))
    # Lexicographic order
    sigma = [0] + [profile + 1 for profile in lexicographic_order]
    # Binary comparisons with preference
    preference_relations = [
        relation
        for relation in relations
        if relation[2] == RelationType.PREFERENCE
    ]
    preference_relations_indices = range(len(preference_relations))
    # Binary comparisons with indifference
    indifference_relations = [
        relation
        for relation in relations
        if relation[2] == RelationType.INDIFFERENCE
    ]
    indifference_relations_indices = range(len(indifference_relations))

    #############
    # Variables #
    #############

    # Weights
    w = LpVariable.dicts("Weight", M, lowBound=0, upBound=1, cat=LpContinuous)
    # Reference profiles
    p = LpVariable.dicts("Profile", (profile_indices, M), cat=LpContinuous)
    # Local concordance to a reference point
    delta = LpVariable.dicts(
        "LocalConcordance",
        (A_star, profile_indices, M),
        cat=LpBinary,
    )
    # Weighted local concordance to a reference point
    omega = LpVariable.dicts(
        "WeightedLocalConcordance",
        (A_star, profile_indices, M),
        lowBound=0,
        upBound=1,
        cat=LpContinuous,
    )
    # Variables used to model the ranking rule with preference relations
    s = LpVariable.dicts(
        "PreferenceRankingVariable",
        (
            preference_relations_indices,
            [0] + profile_indices,
        ),
        cat=LpBinary,
    )

    if inconsistencies:
        # Variables used to model the ranking rule with indifference relations
        s_star = LpVariable.dicts(
            "IndifferenceRankingVariable",
            indifference_relations_indices,
            cat=LpBinary,
        )

    ##############
    # LP problem #
    ##############

    prob = LpProblem("SRMP_Elicitation", LpMaximize)

    if inconsistencies:
        prob += lpSum(
            [s[index][0] for index in preference_relations_indices]
        ) + lpSum([s_star[index] for index in indifference_relations_indices])

    ###############
    # Constraints #
    ###############

    # Normalized weights
    prob += lpSum([w[j] for j in M]) == 1

    for j in M:
        if non_dictator:
            # Non-dictator weights
            prob += w[j] <= 0.5

        # Non-zero weights
        prob += w[j] >= gamma

        # Constraints on the reference profiles
        prob += p[1][j] >= 0
        prob += p[k][j] <= 1

        for h in profile_indices:
            if h != k:
                # Dominance between the reference profiles
                prob += p[h + 1][j] >= p[h][j]

            for a in A_star:
                # Constraints on the local concordances
                prob += (
                    performance_table.loc[a, j] - p[h][j] >= delta[a][h][j] - 1
                )
                prob += (
                    delta[a][h][j]
                    >= performance_table.loc[a, j] - p[h][j] + gamma
                )

                # Constraints on the weighted local concordances
                prob += omega[a][h][j] <= w[j]
                prob += omega[a][h][j] >= 0
                prob += omega[a][h][j] <= delta[a][h][j]
                prob += omega[a][h][j] >= delta[a][h][j] + w[j] - 1

    # Constraints on the preference ranking variables
    for index in preference_relations_indices:
        if not inconsistencies:
            prob += s[index][sigma[0]] == 1
        prob += s[index][sigma[k]] == 0

    for h in profile_indices:
        # Constraints on the preferences
        for index, relation in enumerate(preference_relations):
            a, b, _ = relation

            prob += lpSum([omega[a][sigma[h]][j] for j in M]) >= (
                lpSum([omega[b][sigma[h]][j] for j in M])
                + gamma
                - s[index][sigma[h]] * (1 + gamma)
                - (1 - s[index][sigma[h - 1]])
            )

            prob += lpSum([omega[a][sigma[h]][j] for j in M]) >= (
                lpSum([omega[b][sigma[h]][j] for j in M])
                - (1 - s[index][sigma[h]])
                - (1 - s[index][sigma[h - 1]])
            )

            prob += lpSum([omega[a][sigma[h]][j] for j in M]) <= (
                lpSum([omega[b][sigma[h]][j] for j in M])
                + (1 - s[index][sigma[h]])
                + (1 - s[index][sigma[h - 1]])
            )

        # Constraints on the indifferences
        for index, relation in enumerate(indifference_relations):
            a, b, _ = relation
            if not inconsistencies:
                prob += lpSum([omega[a][sigma[h]][j] for j in M]) == lpSum(
                    [omega[b][sigma[h]][j] for j in M]
                )
            else:
                prob += lpSum([omega[a][sigma[h]][j] for j in M]) <= (
                    lpSum([omega[b][sigma[h]][j] for j in M])
                    - (1 - s_star[index])
                )

                prob += lpSum([omega[b][sigma[h]][j] for j in M]) <= (
                    lpSum([omega[a][sigma[h]][j] for j in M])
                    - (1 - s_star[index])
                )

    # Solve problem
    status = prob.solve(**solver_args)
    objective = pulp_value(prob.objective)

    # Compute optimum solution
    criteria_weights = {j: pulp_value(w[j]) for j in M}
    profiles = PerformanceTable(
        [[pulp_value(p[h][j]) for j in M] for h in profile_indices],
        columns=M,
    )
    fitness = status == 1
    fitness *= objective / len(relations) if inconsistencies else 1
    result: PreferenceElicitationResult = {
        "criteria_weights": criteria_weights,
        "profiles": profiles,
        "lexicographic_order": lexicographic_order,
        "fitness": fitness,
    }
    return result


def denormalize_value(
    x: NumericValue,
    scale: QuantitativeScale,
) -> Value:
    """Denormalize value by taking the closest prefered value available

    :param x:
    :param scale:
    :return:
    """
    if isinstance(scale, QualitativeScale):
        denormalized_x = get_normalized_scale().transform_to(
            x, scale.quantitative
        )
        closest_prefered_value = (
            min([value for value in scale.values if value >= denormalized_x])
            if scale.preference_direction == PreferenceDirection.MAX
            else max(
                [value for value in scale.values if value <= denormalized_x]
            )
        )
        return scale.label_from_value(closest_prefered_value)
    else:
        return get_normalized_scale().transform_to(x, scale)


def denormalize(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
) -> PerformanceTable:
    """Denormalize performance table with the closest prefered values available

    :param performance_table:
    :param scales: target criteria scales
    :return: denormalized performance table
    """
    functions = {
        criterion: (
            cast(
                Function,
                lambda x, c=criterion: denormalize_value(x, scales[c]),
            )
        )  # https://bugs.python.org/issue13652
        for criterion in scales.keys()
    }
    return apply_criteria_functions(performance_table, functions)


def preference_elicitation(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    relations: List[Relation],
    max_profiles_number: int = None,
    profiles_number: int = None,
    lexicographic_order: List[int] = None,
    inconsistencies: bool = True,
    gamma: NumericValue = 0.001,
    non_dictator: bool = False,
    solver_args: dict = None,
) -> PreferenceElicitationResult:
    """Compute the preference elicitation algorithm

    :param performance_table:
    :param scales:
    :param relations: pairwise relations between alternatives
    :param max_profiles_number: highest number of reference profiles
    :param profiles_number: number of reference profiles
    :param lexicographic_order: profile indices used sequentially to rank
    :param inconsistencies:
        if ``True`` inconsistent comparisons will be taken into account
    :param gamma: value used for modeling strict inequalities
    :param non_dictator: if ``True`` prevent dictator weights (> 0.5)
    :param solver_args: extra arguments supplied to the solver
    :return: the inferred criteria weights, profiles, lexicographic order,
        and the proportion of fulfilled comparisons
    :raise TypeError:
        * if `max_profiles_number`, `profiles_number`
          and `lexicographic_order` are not specified
        * if two or more of `max_profiles_number`, `profiles_number`
          and `lexicographic_order` are specified at the same time

    .. note::
        One and only one of `max_profiles_number`, `profiles_number`
        and `lexicographic_order` must be specified
    """
    # Check parameters provided
    provided = (
        (max_profiles_number is not None)
        + (profiles_number is not None)
        + (lexicographic_order is not None)
    )
    if provided == 0:
        raise ValueError(
            "You must specify either 'max_profiles_number',\
            'profiles_number' or 'lexicographic_order'"
        )
    if provided > 1:
        raise ValueError(
            "Only one of 'max_profiles_number', 'profiles_number'\
            or 'lexicographic_order' must be specified"
        )

    if lexicographic_order is not None:
        # Normalize profile values
        norm_table = normalize(
            performance_table, cast(Dict[str, Scale], scales)
        )

        # Compute the preference elicitation algorithm
        result = normalized_preference_elicitation(
            performance_table=norm_table,
            relations=relations,
            lexicographic_order=lexicographic_order,
            inconsistencies=inconsistencies,
            gamma=gamma,
            non_dictator=non_dictator,
            solver_args=solver_args,
        )
        # Denormalize profile values
        profiles = result["profiles"]
        profiles = denormalize(profiles, scales)
        result["profiles"] = profiles
    else:
        # Recursively call elicitation with all possible lexicographic order
        profiles_number_list = (
            list(range(1, max_profiles_number + 1))
            if max_profiles_number is not None
            else []
        )
        lexicographic_order_list = (
            list(permutations(range(profiles_number)))
            if profiles_number is not None
            else []
        )
        # Indices to iterate over profiles_number_list
        # or lexicographic_order_list
        indices = (
            range(len(profiles_number_list))
            if max_profiles_number is not None
            else range(len(lexicographic_order_list))
        )
        best_result: PreferenceElicitationResult = {
            "criteria_weights": {},
            "profiles": PerformanceTable(),
            "lexicographic_order": [],
            "fitness": 0.0,
        }
        for i in indices:
            current_lexicographic_order = (
                list(lexicographic_order_list[i])
                if profiles_number is not None
                else None
            )
            current_profiles_number = (
                profiles_number_list[i]
                if max_profiles_number is not None
                else None
            )
            result = preference_elicitation(
                performance_table=performance_table,
                scales=scales,
                relations=relations,
                profiles_number=current_profiles_number,
                lexicographic_order=current_lexicographic_order,
                inconsistencies=inconsistencies,
                gamma=gamma,
                non_dictator=non_dictator,
                solver_args=solver_args,
            )
            if result["fitness"] > best_result["fitness"]:
                best_result = result.copy()
            if best_result["fitness"] == 1:
                # Break recursion when a perfect solution is found
                break
        result = best_result
    return result


##################
# Plot functions #
##################


def plot_input_data(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    criteria_weights: Dict[str, NumericValue] = None,
    profiles: PerformanceTable = None,
    lexicographic_order: List[int] = None,
    annotations: bool = False,
    annotations_alpha: float = 0.5,
    scales_boundaries: bool = False,
    figsize: Tuple[float, float] = None,
    xticklabels_tilted: bool = False,
    **kwargs,
):
    """Visualize input data
    For each criterion, the arrow indicates the preference direction.
    The criteria weights are displayed as a bar plot,
    and their values are written in parentheses

    :param performance_table:
    :param scales:
    :param criteria_weights:
    :param profiles:
    :param lexicographic_order: profile indices used sequentially to rank
    :param annotations:
        if ``True`` every point is annotated with its value
    :param annotations_alpha: annotations white box transparency
    :param scales_boundaries:
        if ``True`` the criteria boundaries are the scales boundaries,
        else they are computed from the data
    :param figsize: figure size in inches as a tuple (`width`, `height`)
    :param xticklabels_tilted:
        if ``True`` `xticklabels` are tilted to better fit
    """
    # Reorder scales and criteria_weights
    scales = {crit: scales[crit] for crit in performance_table.columns}
    if criteria_weights is not None:
        criteria_weights = {
            crit: criteria_weights[crit] for crit in performance_table.columns
        }

    # Transform to quantitative scales
    quantitative_scales = {}
    for key, scale in scales.items():
        if isinstance(scale, QualitativeScale):
            quantitative_scales[key] = scale.quantitative
        else:
            quantitative_scales[key] = scale

    # Normalize data
    if profiles is not None:
        df = concat([performance_table, profiles])
    else:
        df = performance_table.copy()
    df = transform(
        df,
        cast(Dict[str, Scale], scales),
        cast(Dict[str, Scale], quantitative_scales),
    )
    if scales_boundaries:
        df = normalize(df, cast(Dict[str, Scale], quantitative_scales))
    else:
        df = normalize_without_scales(df)
        for key, scale in quantitative_scales.items():
            if scale.preference_direction == PreferenceDirection.MIN:
                df[key] = 1 - df[key]

    # Create constants
    nb_alt = len(performance_table)
    if profiles is not None:
        nb_profiles = len(profiles)

    # Create figure and axis
    fig = Figure(figsize=figsize)
    ax = fig.create_add_axis()

    # Axis parameters
    x = cast(List[float], range(len(performance_table.columns)))
    xticks = cast(List[float], (range(len(performance_table.columns))))
    if criteria_weights is not None:
        xticklabels = [
            f"{crit}\n({weight})" for crit, weight in criteria_weights.items()
        ]
    else:
        xticklabels = [f"{crit}" for crit in performance_table.columns]

    # Criteria weights
    if criteria_weights is not None:
        weights = np.array([*dict(criteria_weights).values()])
        weights_normalized = weights / weights.sum()
        ax.add_plot(
            BarPlot(
                x,
                weights_normalized,
                xticks=xticks,
                yticks=[],
                xticklabels=xticklabels,
                xticklabels_tilted=xticklabels_tilted,
                width=0.1,
                alpha=0.5,
            )
        )

    # Plotted annotations' coordinates
    annotations_coord: List[Tuple[float, float]] = []

    # Profiles
    if profiles is not None:
        for profile in range(nb_alt, nb_alt + nb_profiles):
            ax.add_plot(
                AreaPlot(
                    x,
                    df.iloc[profile],
                    xticks=xticks,
                    yticks=[],
                    xticklabels=xticklabels,
                    xticklabels_tilted=xticklabels_tilted,
                    color="black",
                    alpha=0.1,
                    strongline=False,
                )
            )
            ax.add_plot(
                Annotation(
                    0,
                    df.iloc[profile, 0],
                    f"$P^{profile-nb_alt}$",
                    -1,
                    0,
                    "right",
                    "center",
                )
            )

    # Alternatives
    values = df[:nb_alt]
    labels = df[:nb_alt].index
    ax.add_plot(
        ParallelCoordinatesPlot(
            x,
            values,
            xticks=xticks,
            yticks=[],
            xticklabels=xticklabels,
            xticklabels_tilted=xticklabels_tilted,
            labels=labels,
            linestyle="-.",
        )
    )
    # Legend
    ax.add_legend(title="Alternatives :", location="right")

    fig.draw()
    assert ax.ax is not None  # to comply with mypy

    # Annotations
    if annotations:
        if profiles is not None:
            for profile in range(nb_alt, nb_alt + nb_profiles):
                for i in x:
                    xy = (i, df.iloc[profile, i])
                    overlap = False
                    for (xc, yc) in annotations_coord:
                        if (xc == i) and (
                            abs(
                                ax.ax.transData.transform(xy)[1]
                                - ax.ax.transData.transform((xc, yc))[1]
                            )
                            < 20
                        ):
                            # if current annotation overlaps
                            # already plotted annotations
                            overlap = True
                            break

                    if not overlap:
                        annotation = Annotation(
                            i,
                            df.iloc[profile, i],
                            profiles.iloc[profile - nb_alt, i],
                            2,
                            0,
                            "left",
                            "center",
                            annotations_alpha,
                        )
                        ax.add_plot(annotation)
                        annotations_coord.append((i, df.iloc[profile, i]))

        for alt in range(nb_alt):
            for i in x:
                xy = (i, df.iloc[alt, i])
                overlap = False
                for (xc, yc) in annotations_coord:
                    if (xc == i) and (
                        abs(
                            ax.ax.transData.transform(xy)[1]
                            - ax.ax.transData.transform((xc, yc))[1]
                        )
                        < 20
                    ):
                        # if current annotation overlaps
                        # already plotted annotations
                        overlap = True
                        break

                if not overlap:
                    annotation = Annotation(
                        i,
                        df.iloc[alt, i],
                        performance_table.iloc[alt, i],
                        2,
                        0,
                        "left",
                        "center",
                        annotations_alpha,
                    )
                    ax.add_plot(annotation)
                    annotations_coord.append((i, df.iloc[alt, i]))

    # Lexicographic order
    if lexicographic_order is not None:
        text = Text(
            0,
            1.2,
            "Lexicographic order : $"
            + r" \rightarrow ".join(
                [f"P^{profile}" for profile in lexicographic_order]
            )
            + "$",
            box=True,
        )
        ax.add_plot(text)
    fig.draw()


def plot_concordance_index(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    criteria_weights: Dict[str, NumericValue],
    profiles: PerformanceTable,
    lexicographic_order: List[int],
    figsize: Tuple[float, float] = None,
    ncols: int = 0,
    nrows: int = 0,
    xlabels_tilted: bool = False,
    **kwargs,
):
    """Visualize concordance index between alternatives and profiles

    :param performance_table:
    :param scales:
    :param criteria_weights:
    :param profiles:
    :param lexicographic_order: profile indices used sequentially to rank
    :param figsize: figure size in inches as a tuple (`width`, `height`)
    :param xlabels_tilted:
        if ``True`` `xlabels` are tilted to better fit
    """
    # Create constants
    nb_alt = len(performance_table)
    nb_profiles = len(profiles)
    weights_sum = sum(dict(criteria_weights).values())

    # Create figure and axes
    fig = Figure(figsize=figsize, ncols=ncols, nrows=nrows)

    for ind_alt in range(nb_alt):
        ax = Axis(
            xlabel=f"{performance_table.index[ind_alt]}",
            xlabel_tilted=xlabels_tilted,
        )
        # Axis properties
        x = cast(List[float], range(nb_profiles))
        xticks = cast(List[float], range(nb_profiles))
        xticklabels = [f"$P^{profile}$" for profile in lexicographic_order]
        ylim = (0.0, 1.0)

        values = []
        # Draw the stacked barplot
        for ind_crit, crit in enumerate(performance_table.columns):
            crit_values = np.array(
                [
                    criteria_weights[crit] / weights_sum
                    if is_better_or_equal(
                        performance_table.iloc[ind_alt, ind_crit],
                        profiles.iloc[profile, ind_crit],
                        scales[crit],
                    )
                    else 0
                    for profile in lexicographic_order
                ]
            )
            values.append(crit_values)
        ax.add_plot(
            StackedBarPlot(
                x,
                values,
                ylim=ylim,
                xticks=xticks,
                xticklabels=xticklabels,
                labels=performance_table.columns,
            )
        )
        fig.add_axis(ax)
    ax.add_legend(title="Criteria :", location="right")
    fig.draw()


def plot_progressive_ranking(
    performance_table: PerformanceTable,
    scales: Dict[str, QuantitativeScale],
    criteria_weights: Dict[str, NumericValue],
    profiles: PerformanceTable,
    lexicographic_order: List[int],
    figsize: Tuple[float, float] = None,
    **kwargs,
):
    """Visualize ranking progressively according to the lexicographic order

    :param performance_table:
    :param scales:
    :param criteria_weights:
    :param profiles:
    :param lexicographic_order: profile indices used sequentially to rank
    :param figsize: figure size in inches as a tuple (`width`, `height`)
    """
    # Create constants
    nb_alt = len(performance_table)
    nb_profiles = len(lexicographic_order)

    # Compute rankings progressively
    relations = profiles.apply(
        preference_relation,
        1,
        performance_table=performance_table,
        criteria_weights=criteria_weights,
        scales=scales,
    ).tolist()
    rankings = PerformanceTable(
        [
            ranking(relations, lexicographic_order[:stop])
            for stop in range(1, nb_profiles + 1)
        ]
    )

    # Compute ranks
    final_values = (
        rankings.iloc[nb_profiles - 1].drop_duplicates().sort_values()
    )
    value_to_rank = {
        value: rank
        for value, rank in zip(final_values, range(1, len(final_values) + 1))
    }
    ranks = rankings.applymap(lambda x: value_to_rank[x])
    nb_ranks = len(value_to_rank)

    # Create figure and axes
    fig = Figure(figsize=figsize)
    ax = Axis(xlabel="Profiles", ylabel="Rank")
    fig.add_axis(ax)

    # Axis parameters
    xticks = cast(List[float], range(nb_profiles))
    xticklabels = [f"$P^{profile}$" for profile in lexicographic_order]
    ylim = (0.5, nb_ranks + 0.5)
    yticks = cast(List[float], range(1, nb_ranks + 1))
    yminorticks = np.arange(1, nb_ranks + 2) - 0.5
    yticklabels = cast(List[str], range(nb_ranks, 0, -1))

    # Draw horizontal striped background
    ax.add_plot(
        HorizontalStripes(
            yminorticks, color="black", alpha=0.1, attach_yticks=True
        )
    )

    # Number of alternatives for each rank (depending on the profile)
    rank_counts = PerformanceTable(
        [
            dict(zip(*np.unique(ranks.loc[profile], return_counts=True)))
            for profile in ranks.index
        ],
        columns=range(1, nb_alt + 1),
    ).fillna(0)
    # Offsets' width for each rank (depending on the profile)
    offsets_width = 1 / (rank_counts + 1)
    # Offsets to apply to current alternative's ranks
    offsets = [0.5] * nb_profiles
    # Alternatives sorted according to the final ranking
    final_ranking_sorted = rankings.iloc[-1].sort_values(ascending=False).index
    # Previous alternative's ranks
    previous_ranks = [0] * nb_profiles

    for alt in final_ranking_sorted:
        # Current alternative's ranks
        current_ranks = ranks[alt]
        # Update offsets (return to 0.5 if it's a new rank)
        offsets = np.where(current_ranks == previous_ranks, offsets, 0.5)
        offsets = [
            offsets[profile]
            - offsets_width.loc[profile, current_ranks[profile]]
            for profile in range(nb_profiles)
        ]
        x = cast(List[float], range(nb_profiles))
        y = current_ranks + offsets
        ax.add_plot(
            LinePlot(
                x,
                y,
                xticks=xticks,
                xticklabels=xticklabels,
                ylim=ylim,
                yticks=yticks,
                yticklabels=yticklabels,
                marker="o",
            )
        )
        ax.add_plot(
            Annotation(
                nb_profiles - 1,
                current_ranks.iloc[-1] + offsets[-1],
                str(alt),
                10,
                0,
                vertical_alignement="center",
                box=True,
            )
        )
        previous_ranks = current_ranks
    fig.draw()
