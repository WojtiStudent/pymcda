# 1. Introduction

This file offers guidelines on how to install everything necessary to install this package.
However it is targeted towards contributors, as users can simply install it using `pip install mcda`.

Contributors can clone the repository on their local machine.

The `dev` branch is used as the central most up-to-date branch during development.


# 2. Set up pyenv & virtualenv

In order to limit the possible conflicts with any other projects, it is recommended
you set up [pyenv](https://github.com/pyenv/pyenv) and a [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) or any other mean of containing this project and keep it separate. 
You can follow the installation instructions of [pyenv](https://github.com/pyenv/pyenv#installation), then [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv#installation).
The following assume you have installed both.

This project uses python 3.6.13, if you haven't installed it, run the following command:

```bash
pyenv install 3.6.13
```

Now you can set up a new virtualenv to contain this project, place yourself in the project root directory and run the following commands (replacing `pymcda-env` by any name you want to give this new virtual environment:

```bash
pyenv virtualenv 3.6.13 pymcda-env
pyenv local pymcda-env
```

The second line binds the project directory with the created virtual environment.
The new virtual environment all is set up! All it remains is to install dependencies!


# 3. Installing dependencies

To install all dependencies, run the following:

```bash
make install
```

Alternatively, you can install the package directly in editable mode, by running the following (replacing `/absolute/path/to/` by the package root folder):

```bash
pip install -e /absolute/path/to/pymcda
```

**N.B: this will however only install the required packages to use the package, not the development required tools!**

That's it!


# 4. Using the project

Users and developers, please read the [LICENSE](LICENSE) file before reusing this code outside the scope of this project.
You can also visit [this](https://py-mcda.readthedocs.io/) or alternatively open *doc/html/index.html* to access the complete documentation of this project.
