
# Table of contents

[TOC]


# Repository

The git repos we are maintaining use multiple branches with a specific scope:

* `master`: for release code usable by the public
* `dev`: for the code being developed (between releases)
* `feature/*`: all temporary branches called `feature/feature_name` focusing on the development of a single feature (when feature in question requires a lot of development)
* `fix/*`: all temporary branches used when working on fixes to issues tracked on the repo website (they are called `fix/n` with `n` the number of the issue on the bug tracker)

**The common `master` and `dev` branches are protected and developers must not work directly on them.**

When starting a new development, you should always span a new temporary branch from the common `dev` branch.
**We strongly recommend that each new feature development or bug fix gets its own branch.**

When you have checked extensively the modifications made on a feature branch, you can then merge them on the common `dev` branch. As a matter of fact, always check extensively your changes before merging them on any common branch.
This merge should be made using a gitlab merge request.

After each block of code implementing a feature that has been checked, you **must**, **without exception**, commit and push.

We provide an example of a pre-commit hook in order to automatically check changes follow coding conventions before allowing a commit. It checks in the following order:

* [linters](#linters)
* [unit tests](#unit-testing)

```bash
#!/bin/bash

# Immediately exit on error
set -o errexit -o pipefail

# Redirect output to stderr.
exec 1>&2

# Check linters and tests before commit
make lint test
```

The previous hook is quite verbose as it always prints out the whole process and result of the linters. If you want it to be only printed on failure, use this hook instead:

```bash
#!/bin/bash

# Check linters and tests before commit
# Only return stdout and complete stderr when it fails
{
    PRINTOUT=$(make lint test 2>&1)
} || {
    echo "$PRINTOUT"
    exit 1    
}
```

Those hooks use commands set up by a [Makefile](#makefile).

To use one of these hooks, copy it in `.git/hooks/` as `pre-commit` and make sure it has execution rights.


## Commits

Your commits should be frequent and atomic (**never bundle lots of changes to multiple features inside a single commit**). If you have made a lot of changes, you can add file changes to the commit interactively using `git add -p`.

**Do not underestimate the value of a clear-cut, well formatted commit message.**
We recommend the following guidelines to be used when writing a commit message: <https://chris.beams.io/posts/git-commit/>

For the format of the commit message, we demand the following:

* First line (header): commit short message (again see <https://chris.beams.io/posts/git-commit/>)
* Body: details about the commit (if needed), written as complete sentences
* Last line (if applicable): references to the issue(s) related to this commit (ex: `See #1 #7` or `Fix #5`)


## Tags

Tags are used (among other things) to indicates the commit number of a specific version of the repo. This is important to put in place once the repo is made available to the public so users and developers alike can track versions.

We use three numbers identifying a version: `a.b.c`

* `a`: indicates major revisions (lot of changes perhaps with no backward compatibility)
* `b`: indicates minor revisions (new feature introduced or a bunch of smaller ones)
* `c`: indicates patch or bug-fix

Those version numbers should be at least used for the `master` branch, and can indicate releases. The tags indicating versions should be formatted: `v{a}.{b}.{c}` (for example: `v1.0.1`)

You can get the commits associated with each tag, which gives you the version number of these commits.
But you can also get the version number associated with any commit in the history, by calling the following command (having checkout the commit to version):

`git describe --tags --match "v[0-9]*"`

This command will generate a version number with the following format: `LAST_TAG_NAME-NB_COMMITS-COMMIT-CHECKSUM` with `LAST_TAG_NAME` the closest tag in the commit history, `COMMIT_CHECKSUM` the checksum of the current commit and `NB_COMMIT` the number of commits separating the commit from the tag.
For example in our case, with a commit checksum "gfface8d", 3 commits after commit tagged with version "v1.2.0", we get: `v1.2.0-3-gfface8d`


## Changelog

A changelog file `CHANGES` must be maintained, detailing the changes brought about by a new revision. It is recommended you modify it along your other commits to not miss anything.
Each set of changes is associated to a version number, which is associated to a [tag](#tags).

We are using the following format:

```
...

v{a}.{b}.{c+1}
--------------

- change1
  - subchange1
  ...
- change2
...


v{a}.{b}.{c}
------------
(start of the changelog)
```

So each new version changes are appended above the changelog.

Between two releases, new changes are added to the top without a version number as section header (as version number still need to be determined).
The addition as a version number as the top section header must be done upon releasing a new version.


## Package requirements

It is mandatory that you keep an up-to-date file `requirements.txt` at the root of your project directory containing the package dependencies (which you can obtain through `pip freeze > requirements.txt`).

In this purpose, it is best to have set a python virtual environment to not gather requirements of other projects. If you need help setting that up, see section [Python environment](#python-environment).


## Package structure

This package uses the following structure:

```
.
├── README                             # package readme in .md or .rst format
├── CHANGES                            # package changelog txt format
├── INSTALL.md                         # installation instructions for git cloners
├── Makefile                           # contains all helpers/utilities for developers
├── requirements.txt                   # up-to-date list of python packages and versions required (obtained with `pip freeze > requirements.txt`)
├── MANIFEST.in                        # indicates all non-source files to include when building package
├── pyproject.toml                     # Minimal configuration to build package distributed binaries
├── .gitlab-ci.yml                     # Continuous Integration script for gitlab pipeline
├── .readthedocs.yaml                  # Configuration for readthedocs integration
├── setup.py                           # package installation setup
├── mcda                               # folder for python sources structured for packaging
│   .
├── tests                              # folder containing python tests (unit or coverage)
│   .
├── doc                                # documentation folder
│   .
│   └── html                           # html documentation folder
├── LICENSE                            # present package intent, license choice and authors (english)
├── LICENCE                            
│   ├── {complete_license_file}_EN.txt # complete license file in english
│   ├── {complete_license_file}_FR.txt # complete license file in french
│   ├── LICENCE.en                     # present package intent, license choice and authors (english)
│   └── LICENCE.fr                     # present package intent, license choice and authors (french)
├── examples                           # folder containing all notebooks supplied as examples
│   .
```


# Workflow

## Geting started

If you want to start contributing to this project, you will need to set up your development environment.

1. First start by cloning this repository
2. Then **we recommend that you create a separate python environment for this project** using for example *pyenv and pyenv-virtualenv* (see [Python environment](#python-environment) for mre details)
3. You can then install the required python packages: `make install`
4. **We strongly recommend you set up a pre-commit hook** so that you can test your changes locally automatically when commiting (see [this](#repository))

You are ready to go!
If you are unsure, you can run some of the tests included in the [Makefile](#makefile).


## Development

As mentionned earlier, the `dev` branch is the most up-to-date version of the project.
You should always start from this branch when developing a new feature or bugfix.
And any change made shall ultimately be merged on this branch.


### Identify an issue

It is a good practice to keep an issue and bug tracker as complete and up-to-date as possible (see [section](#bug-tracking)).

So it is recommended you start any development by creating a new issue, or selecting an open issue on the gitlab repository.
You can then assign this issue to yourself so other contributors know this issue is being taken care of.


### Create temporary branch

You should create a branch specific to the development you want to do, this branch shall spans from the up-to-date `dev` branch:

```bash
git checkout dev              # get on 'dev' branch
git pull                      # update branch so it matches its remote state
git checkout -b feature/xxxx  # create and get on a feature branch (when developing a new feature 'xxxx')
git checkout -b fix/n         # create and get on a bugfix branch (when fixing a bug with issue number {n')
```

### Working on temporary branch

When working, remember to commit and push your changes often.
**If working on a specified issue, always reference that issue in the commit messages** (see [this](#commits)).
**Also whenever an issue has been resolved, mention it in the message of the commit solving the issue.**
We recommend that you develop alongside the followings:

* source code
* documentation (see [this](#code-documentation) for more details)
  * doc-string (see [this](#doc-strings) for more details)
  * type hints (see [this](#type-hinting) for more details)
  * comments (see [this](#comments) for more details)
* unit tests (see [this](#unit-testing) for more details)
* notebooks showing how a high-level use of your newly added features (see [this](#notebooks) for more details)

We also recommend you test your code as often as possible, using the provided [Makefile](#makefile) targets.

You can keep up with other branches progress, using the following command: `git fetch`


### Merging branch on common dev branch

When your development is over and you think it can be merged back into the `dev` branch, you shall check the following:

1. Your changes actually accomplish what they were intended (e.g the issue related to the branch is resolved)
2. Your code passes the [linters](#linters)
3. The [documentation](#code-documentation) of your changes is complete
4. The [unit tests](#unit-testing) are working and [cover](#coverage-testing) all your changes (*exception tolerated for plotting functions*)
5. The [notebooks](#notebooks) cover your changes (recommended but not mandatory)
6. The changes have been appended to the top of the [changelog](#changelog)
7. The package requirements are up-to-date (both in the [requirements.txt](requirements.txt) and [setup.py](setup.py) files)
8. All your changes have been pushed on the online repository

Then you can open a new merge request on gitlab, requesting the merge of your branch on the `dev` branch.
Please use the following options:

* Delete source branch after merge
* **Do not stash changes before merge**

Then your merge request will be checked and only applied when approved by a project maintainer.
While the merge request is open, you can add commits to it, and interact with other developers about the request.
Until the merge request is approved and applied, you may have to come back to it and perform some changes.

However in the meantime, you can start working on a new feature.

We recommend to start from the `dev` branch, however in the case where the previous merge request has not yet been accepted and your new feature will depend on it,
you may start a new branch spanning from your not-yet-merged branch.

When the merge is applied, you can delete your local branch and its remote tracking:

```bash
git branch -d feature/xxxx
git branch -d -r origin/feature/xxxx
```


### Release a new version

When enough features have been added, or if a package-breaking bug has been fixed, you may want to release a new version of the package on [PyPI](https://pypi.org/project/mcda/).
You must then check the followings:

1. Your code passes the [linters](#linters)
2. The [documentation](#code-documentation) of your changes is complete
3. The [unit tests](#unit-testing) are working and [cover](#coverage-testing) all your changes (*exception tolerated for plotting functions*)
4. The [notebooks](#notebooks) cover your changes (recommended but not mandatory)
5. The changes have been appended to the top of the [changelog](#changelog)
6. A version number is associated to the release and appended at the top of the [changelog](#changelog)
7. The package requirements are up-to-date (both in the [requirements.txt](requirements.txt) and [setup.py](setup.py) files)

Then you can open a new merge request on gitlab, requesting the merge of your `dev` on the `master` branch.
Please use the following options:

* Delete source branch after merge
* **Do not stash changes before merge**

Then your merge request will be checked and only applied when approved by a project maintainer.
While the merge request is open, you can add commits to it, and interact with other developers about the request.
Until the merge request is approved and applied, you may have to come back to it and perform some changes.

When the merge request is approved and applied, the gitlab pipeline for the `master` branch will include a new extra job
to deploy the new version of the package on [PyPI](https://pypi.org/project/mcda/) which is triggered manually.

Then to ensure the correspondence between the release on gitlab and PyPI, you need to tag the commit of the release:

```bash
git checkout master
git pull
git tag v{a}.{b}.{c}
git push --tags
```

The tag can also be applied from the gitlab web page.


# Git functionalities

## Clone a remote git repository

To clone a remote git repository, simply execute:

`git clone link/to/repo`

This will download the git repository and checkout the main branch.


## Set-up username and email

Git won't let you commit anything while you have not set up the following constants:

* `user.name`: the username you will use on the online git repository (also will show up in the commits you make)
* `user.email`: your e-mail

Those can be set up globally using:

```bash
git config --global user.name USERNAME
git config --global user.email EMAIL
```

Or for a specific project by executing the following from the git directory:

```bash
git config --local user.name USERNAME
git config --local user.email EMAIL
```


## Set up mergetool and difftool

`git difftool` is a utility to show and resolve the differences between two commits.
`git mergetool` is a utility to show and resolve the conflicts between two commits for an ongoing merge.

You can set up once the text editor (such as `meld`) to use for both utilities using the following commands:

```bash
git config --global diff.tool EDITOR
git config --global merge.tool EDITOR
git config --global difftool.prompt false  # disable prompt before executing difftool
```

Then those commands can be used.
If the commands do not work, you may need to customize the configuration of the editor in the `.gitconfig` file (found in your home directory on unix), depending on the editor you chose.

Example configuration for the editor `meld`:

```ini
# Add the following to your .gitconfig file.
[diff]
    tool = meld
[difftool]
    prompt = false
[difftool "meld"]
    cmd = meld "$LOCAL" "$REMOTE"
[merge]
    tool = meld
[mergetool "meld"]
    # Choose one of these 2 lines (not both!) explained below.
    cmd = meld "$LOCAL" "$MERGED" "$REMOTE" --output "$MERGED"
    cmd = meld "$LOCAL" "$BASE" "$REMOTE" --output "$MERGED"
```

On the last two lines, the difference is about what version of the merged file you want to display in the centre. `$MERGED` will contain the partially merged file and the conflict information while `$BASE` will contain the version before any merging. In both cases, the centre version is the one that will be used upon commiting the merge.


## Create feature or hotfix branches

As mentionned previously, you shall work routinely on a temporary feature branch.
To create it the first time, checkout first the branch you will use as the base (in our case, `dev`), then create your branch spanning from it:

```bash
git checkout dev             # checkout the 'dev' branch
git pull                     # update local version of the branch 'dev'
git checkout -b feature/xxxx # create and checkout a new branch 'feature/xxxx' from current branch ('dev')
```

Same case when working on a hotfix (see [bug tracking](#bug-tracking)).


## Keep local branches up-to-date

There are multiple ways to keep your git local repository up-to-date with the online repository `origin`. The simplest way **if you are well aware of the git structure or are retrieving changes on a branch you have not locally modified** is to execute a pull:

```bash
git checkout BRANCH_NAME  # checkout `BRANCH_NAME` you want to update
git pull                  # pull the changes from the online repository `origin`
```

If you are unsure, you can first fetch the latest version of every branch: `git fetch`
This command will fetch the branches on all online repositories configured (in our case only `origin`).
You can then check the state of the branches using a utility like `gitg`.
To apply the changes of the remote version of a branch to your local version, you can merge them:

```bash
git fetch                         # fetch the list of changes from the repositories (without applying them)
git checkout BRANCH_NAME          # checkout `BRANCH_NAME` you want to update
git merge REMOTE_NAME/BRANCH_NAME # merge the changes from the online repository REMOTE_NAME (e.g 'origin') onto the branch BRANCH_NAME
```


## Work on feature or hotfix branch

When working, you should commit and push your changes as often as possible.
To do that, you should select the changes you want to commit using any of the following:

* `git add FILENAME`: add all changes made to `FILENAME` since last commit (or add `FILENAME` to the tracked files if it wasn't before)
* `git add FOLDER_NAME`: add all changes and all filenames contained in `FOLDER_NAME`
* `git add -u`: add all changes made to tracked files
* `git add -p [OTHER_OPTIONS]`: add all changes interactively, you can select precisely which chunks of code are being commited
* `git add --all`: add all changes and files to match the current working tree (**Not advised except on the initial commit of a new repository**)
* `git mv FILENAME NEW_FILENAME`: move/rename the filename `FILENAME` to `NEW_FILENAME`
* `git rm [--cached] FILENAME`: remove filename `FILENAME` from the tracked files (as well as from the current directory if the option `--cached` is not used)

You can check the status of your git branch at any time: `git status`

If you want to abort your commit operation, you can execute: `git reset HEAD` (Note: it will mess up the `git mv` operations as the files will keep their new name)

When you are satisifed with the changes you have selected, you can perform the commit:

`git commit -m COMMIT_MESSAGE [-m COMMIT_DETAILS] [-m ISSUE_REFS]` (see [this](ttps://chris.beams.io/posts/git-commit/) for help in formatting your commits)

If you have any second thought about your performed commits, you can easily undo them **while they have not been commited** with: `git reset HEAD~` (will cancel the last unpushed commit)

Once your commit performed, if you are happy with the changes, push them:

`git push origin LOCAL_BRANCH_NAME` (for example: `git push origin dev-bob`)


## Rebase of your branch

**N.B: Do not do the following if you don't have a solid understanding of git in general and the branches structure of the repository you work on!**

When you are working on your branch, and changes are being made on the common branch you are based on (e.g `dev`) but you are not ready to merge your changes, or you want your changes to take into account the changes of the common branch retrospectively, you could perform a rebase of your branch on the common branch:

```bash
git checkout dev           # checkout 'dev' branch
git pull                   # make sure the local version of 'dev' is up-to-date with 'origin' repository
git checkout feature/xxxx  # feature branch we want to rebase
git rebase dev             # modify branch 'feature/xxxx' history so its unmerged changes appear after the last `dev` commits
```

After performing a rebase, you may have to **force** the next push you will make of this branch as the online repository will have to also modify previous history to cope with the rebase:

`git push --force origin feature/xxxx` (N.B: **Never force push on a common branch as it will override the remote history of the branch**)

**However this rebase operation modifies the history of the branch on which we are rebasing, so it must never be performed on any common branch.** This would otherwise force all other users with changes spanning from this common branch to update their whole git history, potentially creating conflicts in their previous commits.


## Merge changes on common branch

When you have completed the implementation of a new feature, or made any significant change to the code and you feel confident enough to add them on the common `dev` branch, you should consider merging the branches.
Remember to check you have updated the [changelog](#changelog) file, adding the new feature(s) you developed to it.

First, make sure you have commited and pushed your changes on your working branch.
Then, it is important to make sure you get the up-to-date version of the branch on which you want to merge (e.g `dev`).

```bash
git checkout dev # checkout the 'dev' branch
git pull         # make sure the local version of 'dev' is up-to-date with 'origin' repository
```

You are ready to merge.

You may encounter some conflicts as the `dev` branch may have had some changes since you last split from it or merged to it.
In order to limit those conflicts and keep the git history as clean as possible, you can rebase your branch on the common branch, to take into account any changes into any common branch that has changed since your last merge/split, before merging. In this case, follow the section on [rebase](#rebasing-your-branch).

To perform the actual merge:

```bash
git checkout dev                   # checkout branch 'dev' where we want to merge 'feature/xxxx'
git merge [MODE] feature/xxxx      # merge branch 'feature/xxxx' into 'dev'
```

There are different modes to perform a merge:

* `--ff` (default mode): will attempt fast-forward merge if possible, fail back on a 3-way merge if conflicts are detected
* `--ff-only`: will only perform merge if no conflicts are detected
* `--no-ff`: will always perform a 3-way merge

If any conflicts appear, the merge will be suspended.
You can then resolve the conflicts either by hand by modifying directly the files reported to have conflicts, or by using the command: `git mergetool`
The latter will open the configured editor to ease the resolution of the conflicts, showing one file at a time, both conflicting versions on the sides, and the merged version in the centre.
Once you have resolved all conflicts, you can create a commit for the merge: `git commit -m MESSAGE`

If you want to abort the merge, you can execute: `git merge --abort`

When you have finished merging your changes on any common branch, you should push them **as soon as possible**. Any delay before pushing changes increases the chances of conflicts for you and the other persons working on this branch.
If you are a developer and not a maintainer, **pushing a merge on a common branch will prompt you to issue a merge request**.
This merge request will freeze the push until a maintainer manually validate the changes brought about by this push.
To access the merge request associated with your merge operation, you can open the link returned to you by the push in the terminal.
Alternatively, you can merge your branch and issue a merge request at the same time on `gitlab` website by creating a new merge request, specifying the branches to merge.

N.B: 
* you can ask the merge request to delete the branch being merged on the common branch (default behaviour)
* we do not recommend stashing all changes in one commit when merging

When you merge a feature branch, it should mean the feature implementation is over.
You may then want to delete the local version of this branch in order to keep your number of local branches as small as possible:

```bash
git branch -d BRANCH_NAME                # delete local branch 'BRANCH_NAME'
git branch -d -r REPOSITORY/BRANCH_NAME  # delete remote tracking of 'BRANCH_NAME' from repository 'REPOSITORY' (e.g 'origin')
```


## Repurpose an outdated branch

If for some reasons, you want to work on a branch which is completely outdated but the branch was not attached to an up-to-date branch, and you don't care about the last unmerged commits.
You basically want to create a new branch with the same name, then you should delete the existing branch and create a new one from any commit/branch you want:

```bash
git branch -d BRANCH_NAME  # delete local version of the branch
git checkout XXXX          # checkout a branch or a specific commit as the new starting point
git switch -c BRANCH_NAME  # create and switch to a new branch named `BRANCH_NAME` starting from `XXXX`
```

If you need to push changes to this branch, you should first delete the remote version of the branch, then push your local branch on the remote:

```bash
git push origin --delete BRANCH_NAME  # delete 'origin' remote version of the branch
git push origin BRANCH_NAME           # push local version of the branch on the remote 'origin'
```

N.B: When deleting a remote version of a branch, the previous commits done on this branch will remain on the remote, they will simply not be associated to the branch name anymore.


## Release a new version

When the development done on `dev` branch is stable enough, and significant changes have been made, we can release an new version of the code on the git `master` branch.

It simply requires to merge the `dev` branch on the `master` branch, and then tag this new release with a [version number](#tags) (do not forget to update the [changelog](#changelog) and [requirements](#package-requirements) beforehand):

```bash
git checkout master    # checkout the 'master' branch
git merge --no-ff dev  # merge the 'dev' branch inside in 3-way mode
git tag VERSION        # tag the new release with VERSION
```

If the release process necessitates other steps, you should create a temporary release branch `release/VERSION` (`VERSION` indicating the [version](#tags) being released). You will them perform the steps necessary for releasing the new version on this branch.
After the release is complete, you will merge it on the `master` branch, tag it, then merge it on `dev`, then delete the release branch.

```bash
git checkout dev                         # checkout the 'dev' branch which you want to release
git checkout -b release/VERSION          # create a release branch and checkout it
...                                      # work on the release process
git checkout master                      # checkout 'master' branch
git merge --no-ff release/VERSION        # merge the release branch inside in 3-way mode
git tag VERSION                          # tag the new release with VERSION
git checkout dev                         # checkout 'master' branch
git merge --no-ff release/VERSION        # merge the release branch inside in 3-way mode
git branch -d release/VERSION            # delete the local release branch
git push origin --delete release/VERSION # delete the remote release branch (only if it was pushed)
```

As you can see, the merge for a release is made in 3-way mode. As a matter of fact, **never merge on the `master` branch in fast-forward as it must only contain the commits of each release**. Also **any changes on the `master` branch must come from either the `dev` branch or an hotfix branch** (see [bug tracking](#bug-tracking)).


# Python environments

While you can use the default system-wide python environment when working on a single project, although you can have permission issues due to you system install, when working on multiple projects with different requirements (packages, package versions, python version) you can bump into a bunch of compatibility issues.

In order to limit the possible conflicts with any other projects, it is recommended
you set up [pyenv](https://github.com/pyenv/pyenv) and a [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) or any other mean of containing this project and keep it separate. 
You can follow the installation instructions of [pyenv](https://github.com/pyenv/pyenv#installation), then [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv#installation).

This will enable you to install multiple versions of python and set up virtual environments which contain their own python interpreter and their own python packages.
This way, projects may coexist side-by-side, each with their own set of installed packages and their python version.


# Coding conventions

This package follows the [PEP8](https://www.python.org/dev/peps/pep-0008/) recommendations strictly. For example, we fix the line length of each code line to **79**.

We also document the code as we write it in doc-string (see section on [code documentation](#code-documentation)).

We also write static typed python code using type hints (see section on [type hinting](#type-hinting)).

We also use a list of linters to automatically check we respect those conventions (see section on [linters](#linters)).


## Type hinting

We rely on [type hinting](https://docs.python.org/3/library/typing.html) to add some semblance of a static typed package.

It enables to add information about variables, parameters and function return types directly inline.

This enables at least to document what those types are supposed to be, as this type hinting has no influence at runtime.

Furthermore, some utilities such as [mypy](https://mypy.readthedocs.io/en/stable/) can perform static verification of your code by parsing and checking the coherence of these type hints.

N.B: the type hints are used to tag the functions to verify using mypy, so you can use type hinting on parts of your code you want to check more thoroughly.

N.B: these type hints are parsed by sphinx to complete the autogenerated API documentation.


## Linters

We recommend the following list of tools to automatically check that best coding practices are followed:

* [flake8](#flake8): check [PEP8](https://www.python.org/dev/peps/pep-0008/) rules compliance (install with `pip install flake8`)
* [isort](#isort): check and place the imports order (install with `pip install isort`)
* [black](#black): code formatter (install with `pip install black`)

If using type hinting to write a statically typed project, add [mypy](#mypy) to the list.


### flake8

Usage: `flake8 [OPTIONS] root_src_folder`

Recommended options:

* `--max-line-length n`: set maximum line length to `n` (79 in our case)
* `--ignore=rule_1,rule_2,...`: ignore the given set of rules (recommend E203,W503 for [black](#black) compatibility)


### isort

Usage: `isort [OPTIONS] root_source_folder`

Options when applying changes:
* `--atomic`: to avoid syntax errors when applying changes

Options for checking for changes to be made:
* `--diff`: print the changes to be made instead of applying them directly
* `--check`: check the files for unsorted/unformatted imports and returns boolean result (can be used atop `--diff`)

Options to use in any situation:
* `--profile black`: to make isort operations compatible with [black](#black)
* `--combine-star`: ensure that if a star import is present, nothing else is imported from that namespace
* `--use-parentheses`: use parenthesis for line continuation instead of slashes
* `-m VERTICAL_HANGING_INDENT`: organize multi-line imports so only one import per line is used
* `-w n`: set maximum line length to `n` (as already said, we recommend 79)


### black

Usage: `black [OPTIONS] root_src_folder`

Option when checking for changes to be made:
* `--check`

Option to use in any situation:
* `--line-length n`: set maximum line length to `n` (as already said, we recommend 79)

N.B: For many IDE, black can be directly integrated to auto-format the code.


### mypy

Usage: `mypy [OPTIONS] root_src_folder`

Options for using external packages without type checking:
* `--ignore-missing-imports`: ignore imported modules which have no type hinting stubs


# Code documentation

For maintainability, it is important that **the documentation of the code is being built at the same time the code is developed**. There are different documentation conventions, we will describe the one we chose. This choice of conventions is conditioned by the documentation tool we recommend: [sphinx](https://www.sphinx-doc.org/en/master/). This software is able to generate a complete documentation containing manually written .rst files alongside an autogenerated API obtained by parsing source files doc-strings.


## Doc strings

All python functions and classes should be documented inline, using doc-string in [reST](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) formatting compliant with sphinx. Modules and subpackages can also be documented by placing a doc-string before the first import. The aim of subpackage / module doc-string is to express its intent, and also add any relevant information that cannot be contained in the functions and classes (for example: scripts should be described in those doc-strings).

This doc-strings should at the minimum contain the description of the class constructor / function parameters, return values and types, and intent. It can also be completed with various other information at the developers' discretion (for example: mathematic formulas, todos, example code, etc).

When referencing other classes, types and functions from a doc-string, you should as much as possible use a reST reference to the actual one. The aim being to facilitate the documentation readability above all else.
You should also remember to cite scientific references on which you base your contributions
(see [this](#references) for more details).


## Type hinting in doc

The type hints can be parsed by sphinx to complete the autogenerated API documentation.
They are used to complete the type information of parameters and return types of functions.


## Comments

Comments should be used to inform about the implementation details such as scope intent, explanation of a line of code, etc.

They can be placed on their own line when describing the intent of the following code lines, or at the end of a code line to explain this particular line.


## Example

You can see below an example of a code properly documented:

```python
# src/my_array.py
"""This module is used to perform array computations.

Usage
=====

Sum two arrays using this module as a script:

.. code:: bash

    python my_array.py ARRAY1 ARRAY2 OUTPUT
    
    ARRAY1 and ARRAY2 are two csv files containing arrays.
    OUTPUT is the output csv file.

"""
import argparse

import numpy as np


class MyArray:
    """This class provides a wrapper for :class:`numpy.array`.
    
    :param array: array to wrap
    """
    def __init__(self, array: np.array):
        self.array = array
    
    @classmethod
    def load_csv(cls, filename: str) -> 'MyArray':
        """Load array from csv file.
        
        :param filename: csv file
        """
        return MyArray(np.loadtxt(filename, delimiter=","))
    
    def save_csv(self, filename: str):
        """Save array in csv file
        
        :param filename: csv file
        """
        np.savetxt(filename, self.array, fmt="%g", delimiter=",")

    
def sum_arrays(array1: MyArray, array2: MyArray) -> MyArray:
    """Returns the sum of two arrays.
    
    :param array1:
    :param array2:
    """
    return MyArray(array1.array + array2.array)


if __name__ == "__main__":
    # Configuration of parameters
    parser = argparse.ArgumentParser(
        description="Sum two csv arrays"
    )
    parser.add_argument("array1", help="first array in csv file")
    parser.add_argument("array2", help="second array in csv file")
    parser.add_argument("output", help="output file")
    
    # Parse arguments
    args = parser.parse_args()
    
    # Load input files
    array1 = MyArray.load_csv(args.array1)
    array2 = MyArray.load_csv(args.array2)
    
    # Sum inputs and save output
    res = sum_arrays(array1, array2)
    res.save_csv(args.output)

```


# Tests

There are mainly 2 types of tests that we are using in our python projects:

* unit testing using `pytest`
* coverage testing (actually combined with the unit testing)

**We strongly recommend to develop the tests alongside the code if not before. And when commiting changes, developers must check their changes impact on those tests.**


## Unit testing

We recommend the use of unit testing in projects with an extensive library of code written (i.e python packages). They can be based upon the python package and utility `pytest`.

This package is easy to use and will simply execute every function which name starts with "test" in every python file which starts by "test" (N.B: by default). Those test function must have no parameters. It will then check all the `assert` statements (raise an `AssertionError` if the boolean condition inside is not met), and return the number of test functions that passed and failed.

A good practice is to write one file `test_SOURCE.py` in the folder `test/` per source file `SOURCE.py` in `src/`, and implement one test function per module functions and classes, named after the functions or classes (ex: `test_sum_arrays` for the function `sum_arrays`).

Below an example for the source file [my_array.py](#example):

```python
# test/test_my_array.py
import numpy as np

from .my_array import MyArray, sum_arrays


def test_my_arrays():
    """Test MyArray class."""
    # Test constructor
    a1 = MyArray(np.array([0, 1, 2, 3, 4]))
    a2 = MyArray(np.array([1, 1, 1, 1, 1]))
    assert a1.array.shape[0] == a2.array.shape[0] == 5


def test_sum_arrays():
    """Test sum_arrays function."""
    a1 = MyArray(np.array([0, 1, 2, 3, 4]))
    a2 = MyArray(np.array([1, 1, 1, 1, 1]))
    res = sum_arrays(a1, a2)
    assert res.array.shape[0] == 5
    assert res.array[0] == 1 and res.array[1] == 2 and res.array[2] ==3
    assert res.array[3] == 4 and res.array[4] == 5

```


## Coverage testing

Coverage testing simply checks which lines of code have been reached or not, during the execution of an operation. It can be based upon the `coverage` python package and utility. It can produce detailed reports showing the percentage of code lines reached, and even show precisely the code reached in details.

As it is wrapped around an other process execution, we recommend wrapping it around the other type of testing chosen for this project: unit tests or black box ones.

It is a good practice to tend towards a 100% coverage of the code developed for a project, although in particular cases we can be more lenient depending on the lines unreached.

There are a number of commands you can use with this package:

* `coverage run --source src,test --branch -m pytest`: run coverage of the code contained in `src/` and `test/` over `pytest` unit tests
* `coverage run --source ./tests.sh`: run coverage of the code contained in `src/` over `tests.sh` test scripts (e.g a black box testing script)
* `coverage report`: get the report of the latest coverage done (reading the `.coverage` file)
* `coverage html`: generate the detailed report in html format in `htmlcov/index.html`


# Makefile

For easier usability, we provide a [Makefile](Makefile) for developers.
It defines many commands and their options, the more useful ones being:

* `make lint`: check all linters + mypy
* `make lint-apply`: apply isort and black formatting
* `make test`: perform unit testing
* `make coverage`: perform coverage tests on unit testing
* `make coverage-report`: print the coverage of each source file
* `make coverage-report-html`: show the coverage result in details in folder `htmlcov`
* `make doc`: generate the documentation with sphinx
* `make notebook-test`: execute all notebooks in [examples/](examples/)


# Notebooks

We added [jupyter notebooks](https://jupyter.org/) that can be run as examples in [examples/](examples/). Those examples should be extended as the package grows.
**Don't forget to write these notebooks as you add new features to the package**.

To install jupyter, run: `pip install jupyter`

Then to be able to run the notebooks, go to the *examples/* directory and run the following: `jupyter notebook`

This will open a web-page in your browser, listing all the provided notebooks of this package.

You can quickly test if the notebooks execute without errors: `make notebook-test`

For a better grasp at the real user experience when installing the package, and running the examples notebook,
**we strongly recommend setting up another [virtual environment](#python-environments) for the [examples/](examples/) subdirectory.**
In this new [virtual environment](#python-environments), you will do the following (from [examples/](examples/)):

* install this package in editable mode: `pip install -e ..`
* install jupyter package: `pip install jupyter`


# Gitlab CI/CD pipeline

This repository has an associated pipeline for continuous integration.
It consists of a succession of operations that are applied on each commit and merge-request pushed on the online repository.

This pipeline runs the following jobs on every change pushed:

* Lint: test code readability and coherence (see [linters](#linters))
  * black
  * flake8
  * isort
  * mypy
* Doc
  * doc (check errors when generating the doc)
  * api-test (check API in doc is up-to-date)
* Test
  * coverage tests (run unit tests and compute coverage)
  * coverage html report (same as abobe but outputs the report in html detailed form)

N.B: while it is a good practice to only push commits that are guaranteed to pass those tests for every branch (by running those tests locally beforehand), they are only required to pass for the common `dev` and `master` branch.

In the case of a release (i.e a push on the `master` branch), the following jobs are appended:

* Integration tests
  * install: test if the package can be installed from its source code
  * notebooks: run all notebooks [examples/](examples/) and check they are working
* Build: build the distribution of the package
* Deploy (manually triggered): deploy new version of the package on PyPI ([mcda](https://pypi.org/project/mcda/))


# Bug tracking

This package is hosted on [gitlab](https://gitlab.com/decide.imt-atlantique/pymcda), it uses the provided bug tracker.
Anyone authorized can open a new issue.
When opening an issue, there is a form to fill, detailing the issue.
An issue can be about one of the following:

* a broken feature, bug encountered
* a feature not working as expected or intended
* a feature improvement
* a new feature suggestion

## Properly describe the issue

It is important to describe the issue as clearly as possible:

* The subject must be both clear and concise
* The issue detailed description **must** contain the commit checksum or the [version number](#tags) of the code used when getting the issue
* If the issue is about a bug, the detailed description should also contain all the information necessary to reproduce the bug (script launched, data used)


## Analyze the issue

Not all issues require a quick development to solve, or even any development at all.
However, they all need to be analyzed and answered quickly.

The first task for a manager or a developer is to analyze the issue, its urgency and what it would entail.
Generally the **highest** priority is on the bugs/broken feature on the `master` branch, then on bugs/broken feature on the `dev` branch, finally on the rest.
Based on that, the manager or a developer should decide when this issue will start being worked on, and when it should be resolved (if it must be worked on at all), or at least the conditions needed before resolving it (development of another feature for example, release of a new version, etc).
Then an answer should be posted on the issue page to clearly state this analysis.

If it is decided this issue will not be resolved in the near future, it should be indicated.

If it is decided this issue won't be resolved or is completely irrelevant, it should be indicated too, and the issue should be closed. If not, a developer should be assigned with its resolution.


## Assign the issue

If you are a contributor or even the main contributor managing the online git repository, you can assign the issue to a developer which is then tasked with resolving it.
It is important to assign one issue to only one developer, to avoid unnecessary conflicts very probable when working on the same pieces of code at the same time.

Then the developer can interact with the person posting the issue.

The assigned developer will start by creating a temporary branch, dedicated to the work on this issue (can be a `feature/` branch when developing or expanding a feature, or simply a `fix/` branch referencing the issue, see [this](#repository)).

Note: if the issue originates from the `master` branch, and it prevents the good usage of the code, its resolution is known as a hotfix, and the changes made to resolve it will be brought on the `master` branch **as soon as possible**. Feature extensions or suggestions on the other hand do not require a fast merge on `master` and can be integrated in the regular development cycle on the `dev` branch.

The developer **must** then reference the issue in any commits made related to it (see [this](#commits)).


## Close the issue

Depending on the project management organization, either the assigned developer or a manager reviewing the process, will close the issue once resolved.

Before closing the issue or declaring it resolved, the developer will merge the temporary branch on the branch where the issue originated (e.g `dev` if issue on `dev`, `dev` and `master` if hotfix on `master`).

In both cases, the assigned developer must indicates clearly by a post on the issue when the issue is resolved, along with the commit checksum or [version number](#tags) of the merge commit that solved it.

Alternatively, the assigned developer can mention in the message of the last commit before merging on the common branches that it fixes the issue.
To do that, simply put `Fix #n` on the last line of the commit message (with `n` the issue number).
Then when creating and applying the merge request, the issue will be closed automatically.


# A note on floating point precision

The policy of this package concerning floating point accuracy is the following:
* Numeric computations in the source code return raw results (no rounding except if its is part of an algorithm)
* When checking floats equalities/inequalities, the function `math.isclose` from module `math` should be used
  * with its default parameters if possible
  * otherwise the function documentation should mention those parameters
  * parameters can alternatively be passed by the function so user has control
* Numeric results in unit tests should be checked digits precision (using for example `unittest.TestCase.assertAlmostEqual` function)
  * with its default precision if possible (7 digits)
  * otherwise the tested precision should be mentionned in the source code documentation


# References

As our package is meant primarily for research purposes, it is important that we
include in the documentation bibliography references so users can clearly see what
the code is based on.

We have decided to use the [doc/refs.bib](doc/refs.bib) BibTex file to centralize all references across the whole package.
This way, all references are unique, and any contributor should check if a reference is not already
present before adding it.

Then, we recommend the references being cited in the documentation of the functions or modules
using the extension `sphinxcontrib.bibtex` format:

```
:cite:p:`REF_NAME`
```

(for a reference named `REF_NAME`)
