# --
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

# --
ISORT_OPTS=--profile black --combine-star --use-parentheses -m VERTICAL_HANGING_INDENT -w 79
FLAKE_OPTS=--max-line-length=79 --ignore=E203,W503
SRCS:=setup.py mcda tests

# --
.PHONY: install build deploy deploy-test

install:
	pip install -r requirements.txt

build:
	python -m build

deploy-test: build
	python -m twine upload --repository testpypi dist/*

deploy: build
	python -m twine upload dist/*

# --
.PHONY: isort-check isort-apply flake8 black-check black-apply mypy lint lint-apply doc api

isort-check:
	isort ${ISORT_OPTS} --check --diff ${SRCS}

isort-apply:
	isort --atomic ${ISORT_OPTS} ${SRCS}

flake8:
	flake8 ${FLAKE_OPTS} ${SRCS}

black-check:
	black --line-length 79 --check ${SRCS}

black-apply:
	black --line-length 79 ${SRCS}

mypy:
	mypy --ignore-missing-imports ${SRCS}

lint: flake8 isort-check black-check mypy

lint-apply: isort-apply black-apply

api:
	cd doc
	sphinx-apidoc -M -f -o ./ ../mcda

doc: api
	cd doc
	$(MAKE) html

# --
.PHONY: test clean coverage coverage-report coverage-report-html cov-html notebook-test

test:
	python -m pytest

coverage:
	coverage run --source mcda,tests --branch -m pytest

coverage-check: coverage
	coverage report --fail-under=100

coverage-report: coverage
	coverage report

coverage-report-html cov-html: coverage
	coverage html

notebook-test:
	cd examples
	jupyter nbconvert --to notebook --execute *.ipynb --output-dir tmp

clean:
	-find . -type d -name '__pycache__' -exec rm --recursive --force {} +
	rm -Rf examples/tmp
	coverage erase
