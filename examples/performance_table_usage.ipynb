{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "739f292b",
   "metadata": {},
   "source": [
    "# Performance tables\n",
    "\n",
    "This notebook shows how to use performance table and their functions.\n",
    "Functions tested here revolved around scales and functions applied to performance tables, as well as plotting utilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3a5f6495",
   "metadata": {},
   "outputs": [],
   "source": [
    "%config Completer.use_jedi = False"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0591a3f",
   "metadata": {},
   "source": [
    "# Defining MCDA problem\n",
    "\n",
    "We can define a MCDA problem by its alternatives and criteria.\n",
    "\n",
    "## Alternatives and criteria\n",
    "\n",
    "Alternatives and criteria can be defined by the list of their ids or names as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "cdf51283",
   "metadata": {},
   "outputs": [],
   "source": [
    "alternatives = [\"a01\", \"a02\", \"a03\", \"a04\", \"a05\"]\n",
    "criteria = [\"c01\", \"c02\", \"c03\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f150e5c",
   "metadata": {},
   "source": [
    "## Performance table\n",
    "\n",
    "A performance table is a 2D matrix, where the first dimension represents the alternatives, the second the criteria. In each cell, a value or performance represents the performance of a given alternative for a certain criterion.\n",
    "\n",
    "To define a performance table, simply use a dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ae64914f",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pandas import DataFrame"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "5f30efe5",
   "metadata": {},
   "outputs": [],
   "source": [
    "perfTable = DataFrame(\n",
    "    [\n",
    "        [0, \"medium\", \"+\"],\n",
    "        [0.5, \"good\", \"++\"],\n",
    "        [1, \"bad\", \"-\"],\n",
    "        [0.2, \"medium\", \"0\"],\n",
    "        [0.9, \"medium\", \"+\"]\n",
    "    ],\n",
    "    index=alternatives,\n",
    "    columns=criteria\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5b0d407",
   "metadata": {},
   "source": [
    "N.B: to avoid mistakes, it is best to use the same order of alternatives and criteria for the performance table definition.\n",
    "\n",
    "All functions related specifically to performance tables are in the module `performance_table`.\n",
    "\n",
    "## Criteria Scales\n",
    "\n",
    "In order to have a better grasp at what a performance or value represents, for a certain criterion, we can define criteria scales.\n",
    "Those scales can be of 3 types:\n",
    "\n",
    "### Quantitative scales\n",
    "\n",
    "Quantitative scales represent a numerical interval bounding all possible values using this scale. Also a `preference direction` can be used to precise which values are preferred: `PreferenceDirection.MIN` if smaller values are preferred, `PreferenceDirection.MAX` otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8e6a2a60",
   "metadata": {},
   "outputs": [],
   "source": [
    "from mcda.core.scales import *\n",
    "\n",
    "scale1 = QuantitativeScale(0, 1, PreferenceDirection.MIN)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7736c779",
   "metadata": {},
   "source": [
    "### Qualitative scales\n",
    "\n",
    "Qualitative scales represent a discrete set of possible labels, alongside the set of their corresponding numerical values in order to establish a preference order. As for quantitative scales, a `preference direction` can be defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "2a600953",
   "metadata": {},
   "outputs": [],
   "source": [
    "scale2 = QualitativeScale([\"bad\", \"medium\", \"good\"], [1, 2, 3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8df4499",
   "metadata": {},
   "source": [
    "### Nominative scales\n",
    "\n",
    "Nominative scales a discrete set of possible labels. They are unordered.\n",
    "Using nominative scales is not recommended when performing multi criteria decision analysis, and they should be replaced by qualitative scales as soon as possible in the decision process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "2d7a1ab5",
   "metadata": {},
   "outputs": [],
   "source": [
    "scale3 = NominalScale([\"--\", \"-\", \"0\", \"+\", \"++\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21f079b9",
   "metadata": {},
   "source": [
    "### Bundling the criteria scales\n",
    "\n",
    "We can bundle multiple criteria scales together, one per criterion of our MCDA problem, using a dictionary labelled by criterion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "6b0e481a",
   "metadata": {},
   "outputs": [],
   "source": [
    "scales = {\n",
    "    criteria[0]: scale1,\n",
    "    criteria[1]: scale2,\n",
    "    criteria[2]: scale3\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd2e0a0e",
   "metadata": {},
   "source": [
    "# Computations on performance tables\n",
    "\n",
    "First things first, we need to import the module containing all performance table methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "96c87cd9",
   "metadata": {},
   "outputs": [],
   "source": [
    "from mcda import performance_table as ptable"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f178f799",
   "metadata": {},
   "source": [
    "## Check performances values\n",
    "\n",
    "We can check that all performances in a performance table are inside their respective criterion scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "0d842df0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.is_within_criteria_scales(perfTable, scales)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc56b905",
   "metadata": {},
   "source": [
    "## Transform labels into numerical values\n",
    "\n",
    "We can transform all the values contained in a performance table (per criteria) into other scales. Any transformation between quantitative and/or qualitative scales is possible. Nominal scales need to be converted into a qualitative scale first.\n",
    "\n",
    "Note: for nominal scales, you can either transform them calling the transform function, or simply use a qualitative scale with the same labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "520c70d6",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "scale3b = QualitativeScale([\"--\", \"-\", \"0\", \"+\", \"++\"], list(range(5)))\n",
    "scales = {\n",
    "    criteria[0]: scale1,\n",
    "    criteria[1]: scale2,\n",
    "    criteria[2]: scale3b\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bca970f",
   "metadata": {},
   "source": [
    "Then, we have to define the output scale to which we want to transform our performances:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "c3d43681",
   "metadata": {},
   "outputs": [],
   "source": [
    "num_scales = {\n",
    "    criteria[0]: scale1,\n",
    "    criteria[1]: scale2.quantitative,\n",
    "    criteria[2]: scale3b.quantitative\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67a43d1d",
   "metadata": {},
   "source": [
    "We have everything we need to transform our performances:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "7ee56da0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>c01</th>\n",
       "      <th>c02</th>\n",
       "      <th>c03</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>a01</th>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>3.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a02</th>\n",
       "      <td>0.5</td>\n",
       "      <td>3.0</td>\n",
       "      <td>4.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a03</th>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a04</th>\n",
       "      <td>0.2</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a05</th>\n",
       "      <td>0.9</td>\n",
       "      <td>2.0</td>\n",
       "      <td>3.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "     c01  c02  c03\n",
       "a01  0.0  2.0  3.0\n",
       "a02  0.5  3.0  4.0\n",
       "a03  1.0  1.0  1.0\n",
       "a04  0.2  2.0  2.0\n",
       "a05  0.9  2.0  3.0"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.transform(perfTable, scales, num_scales)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbfbc8a1",
   "metadata": {},
   "source": [
    "## Normalize numerical values\n",
    "\n",
    "Numerical values in a performance table can be normalized, using either the raw data for extracting the boundaries, or the criteria scales.\n",
    "\n",
    "### Scale normalization\n",
    "\n",
    "Scales can be used to normalize numerical values of quantitative and qualitative scales. Nominal scales cannot be used to normalize and must there be replaced by qualitative scales."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "7dfc8ff0",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>c01</th>\n",
       "      <th>c02</th>\n",
       "      <th>c03</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>a01</th>\n",
       "      <td>1.0</td>\n",
       "      <td>0.5</td>\n",
       "      <td>0.75</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a02</th>\n",
       "      <td>0.5</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.00</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a03</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.25</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a04</th>\n",
       "      <td>0.8</td>\n",
       "      <td>0.5</td>\n",
       "      <td>0.50</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a05</th>\n",
       "      <td>0.1</td>\n",
       "      <td>0.5</td>\n",
       "      <td>0.75</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "     c01  c02   c03\n",
       "a01  1.0  0.5  0.75\n",
       "a02  0.5  1.0  1.00\n",
       "a03  0.0  0.0  0.25\n",
       "a04  0.8  0.5  0.50\n",
       "a05  0.1  0.5  0.75"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.normalize(perfTable, scales)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc93dd82",
   "metadata": {},
   "source": [
    "N.B: `preference direction` is used to normalize the data, so the resulting performance table values are ordered by preference (in increasing order).\n",
    "\n",
    "### Normalization on raw data\n",
    "\n",
    "You can also decide to normalize the performance table without providing the criteria scales. The min and max values will be retrieved from the performance table.\n",
    "\n",
    "This code normalizes each value per criterion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "90f57384",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.14</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1.0</td>\n",
       "      <td>0.00</td>\n",
       "      <td>0.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.0</td>\n",
       "      <td>1.00</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "     0     1    2\n",
       "0  0.0  0.14  0.0\n",
       "1  1.0  0.00  0.5\n",
       "2  0.0  1.00  1.0"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "table = DataFrame(\n",
    "    [[0, 25000, -5], [1, 18000, -3], [0, 68000, -1]]\n",
    ")\n",
    "ptable.normalize(table)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41bc9c07",
   "metadata": {},
   "source": [
    "N.B: you can also normalize per alternative using the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "1fc3c967",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.000200</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.000222</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.000015</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          0    1    2\n",
       "0  0.000200  1.0  0.0\n",
       "1  0.000222  1.0  0.0\n",
       "2  0.000015  1.0  0.0"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.normalize_without_scales(table, axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e40e61a",
   "metadata": {},
   "source": [
    "## Apply criteria functions to performances\n",
    "\n",
    "It is possible to apply criteria functions to each of the criteria values in the performance table. Those functions can be defined using lambda functions (to represent constant, affine or any type of function):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "fffc5bf8",
   "metadata": {},
   "outputs": [],
   "source": [
    "f2 = lambda x: 2*x - 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0f335be",
   "metadata": {},
   "source": [
    "However for more complex non-arithmetical functions, we provide a module `pymcda.functions` which contains several usefull classes and methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "713ee33a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from mcda.core.functions import *"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21245610",
   "metadata": {},
   "source": [
    "You can define discrete functions using the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "eb3cec7a",
   "metadata": {},
   "outputs": [],
   "source": [
    "f3 = DiscreteFunction({\"--\": 1, \"-\": 2, \"0\": 3, \"+\": 4, \"++\": 5})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddcc15b5",
   "metadata": {},
   "source": [
    "This function can then simply be called as any python function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "8e42f2bd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f3(\"+\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b2b37be",
   "metadata": {},
   "source": [
    "You can also define piecewise functions using the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "3a2d4c28",
   "metadata": {},
   "outputs": [],
   "source": [
    "intervals = [Interval(0, 2.5, max_in=False), Interval(2.5, 5)]\n",
    "functions = [\n",
    "    lambda x: x,\n",
    "    lambda x: -0.5 * x + 2.0,\n",
    "]\n",
    "f = PieceWiseFunction(intervals, functions)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "266f4f55",
   "metadata": {},
   "source": [
    "There is also a simple way to create a piecewise-linear function from a list of segments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "48bb68f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "f1 = PieceWiseFunction(segments=[\n",
    "    [[0, 1], [0.3, -2]],\n",
    "    [[0.3, -2], [0.6, 0.5]],\n",
    "    [[0.6, 0.5], [1, 5]]\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16a4fa5c",
   "metadata": {},
   "source": [
    "You can also simply call a piecewise function like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "74ce7cc8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.75"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f1(0.8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d5ac317",
   "metadata": {},
   "source": [
    "We can also define a scale for the inputs of a function directly inside the function structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "0de2ce17",
   "metadata": {},
   "outputs": [],
   "source": [
    "f2_prev = f2\n",
    "f2 = ScaledFunction(f2, scale2.quantitative)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc591308",
   "metadata": {},
   "source": [
    "This function defined thusly can be called the same way the original function was. Except it will now check for input values upon calling the function.\n",
    "\n",
    "These functions can also easily be composed with any scale transformation if we want to apply the function to inputs of a different scale.\n",
    "\n",
    "For example, let's change 'f2' so it applies the same function to values transformed from the qualitative scale 'scale2' (so that we can apply 'f2' on the values associated to inputs labels):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "0c345663",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5.5"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f2 = f2.transform_to(scale2)\n",
    "f2(\"good\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06cde43c",
   "metadata": {},
   "source": [
    "Those functions, like the scales, can be bundled together in a dictionary, labelled by criterion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "b6ea83e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "functions = {\n",
    "    criteria[0]: f1,\n",
    "    criteria[1]: f2,\n",
    "    criteria[2]: f3\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9783fbd9",
   "metadata": {},
   "source": [
    "Then we can apply the criteria functions to a performance table like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "a209730c",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>c01</th>\n",
       "      <th>c02</th>\n",
       "      <th>c03</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>a01</th>\n",
       "      <td>1.000000</td>\n",
       "      <td>3.5</td>\n",
       "      <td>4</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a02</th>\n",
       "      <td>-0.333333</td>\n",
       "      <td>5.5</td>\n",
       "      <td>5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a03</th>\n",
       "      <td>5.000000</td>\n",
       "      <td>1.5</td>\n",
       "      <td>2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a04</th>\n",
       "      <td>-1.000000</td>\n",
       "      <td>3.5</td>\n",
       "      <td>3</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>a05</th>\n",
       "      <td>3.875000</td>\n",
       "      <td>3.5</td>\n",
       "      <td>4</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          c01  c02  c03\n",
       "a01  1.000000  3.5    4\n",
       "a02 -0.333333  5.5    5\n",
       "a03  5.000000  1.5    2\n",
       "a04 -1.000000  3.5    3\n",
       "a05  3.875000  3.5    4"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nTable = ptable.apply_criteria_functions(perfTable, functions)\n",
    "nTable"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "373f482f",
   "metadata": {},
   "source": [
    "## Check performance table numericity\n",
    "\n",
    "We can easily check if all the values inside a performance table are numeric, using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "608f7b81",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.is_numeric(nTable)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8519742",
   "metadata": {},
   "source": [
    "## Sum performances\n",
    "\n",
    "We can compute the sum of the values of a numerical performance table.\n",
    "The parameter `axis` controls how the sum is computed.\n",
    "This sum can be on the whole performance table (`axis` let to default value):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "b66fccc4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "44.04166666666667"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.sum_table(nTable)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d15148bc",
   "metadata": {},
   "source": [
    "It can also be done column-wise, returning the sums of each column as a list. This corresponds to the sum of all criteria values per alternative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "9daadfa9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "c01     8.541667\n",
       "c02    17.500000\n",
       "c03    18.000000\n",
       "dtype: float64"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.sum_table(nTable, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eef0f1e6",
   "metadata": {},
   "source": [
    "It can also be done row-wise, returning the sums of each row as a list. This corresponds to the sum of all alternatives values per criterion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "8ebb7d62",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "a01     8.500000\n",
       "a02    10.166667\n",
       "a03     8.500000\n",
       "a04     5.500000\n",
       "a05    11.375000\n",
       "dtype: float64"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ptable.sum_table(nTable, axis=1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
