{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f64b3f47",
   "metadata": {},
   "source": [
    "First we configure the notebook so we can use auto-completion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b032368c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%config Completer.use_jedi = False"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86a7f595",
   "metadata": {},
   "source": [
    "We can then import the functions of the `set_functions` module. We will also need some functions of the sets module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ab22390a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from mcda.core.sets import *\n",
    "from mcda.core.set_functions import *"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "795ef3ae",
   "metadata": {},
   "source": [
    "# Set functions\n",
    "\n",
    "## Representation\n",
    "\n",
    "Sets functions are functions defined on a power set. This means they have a value for each possible set in an ensemble. In this package, we simply define those functions by a list of values. The values are written in the same order than the sets integer representation.\n",
    "\n",
    "Say we have the following set function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "1f703de6",
   "metadata": {},
   "outputs": [],
   "source": [
    "f = [0, 0.5, 2, -3]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb24c26b",
   "metadata": {},
   "source": [
    "This function is defined on the power set or ensemble of size 2 (2 elements). We can then get the value of the function for a set using its integer index. Below an example where we see its value for the complete set (both elements in the set):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "041a3dfd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-3"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f[mask_to_int(\"11\")]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72550444",
   "metadata": {},
   "source": [
    "# Usage\n",
    "\n",
    "We have a lot of functions defined for set functions. We can split them in different categories:\n",
    "* Check type constraints of set functions\n",
    "* Analysis of aggregation represented in set functions\n",
    "* Transformations\n",
    "\n",
    "But before continuing, let's define a set function that will be our example for the following. It is a normal capacity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f21b782f",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.07692307692307693,\n",
       " 0.15384615384615385,\n",
       " 0.38461538461538464,\n",
       " 0.23076923076923078,\n",
       " 0.46153846153846156,\n",
       " 0.6153846153846154,\n",
       " 0.8461538461538461,\n",
       " 0.3076923076923077,\n",
       " 0.5384615384615384,\n",
       " 0.6923076923076923,\n",
       " 0.9230769230769231,\n",
       " 0.7692307692307693,\n",
       " 1.0,\n",
       " 1,\n",
       " 1]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l = [i / 13 for i in range(14)] + [1, 1]\n",
    "capacity = [0 for _ in range(len(l))]\n",
    "for i, index in enumerate(cardinal_range(len(l))):\n",
    "    capacity[index] = l[i]\n",
    "capacity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "964df946",
   "metadata": {},
   "source": [
    "N.B: We are instanciating the capacity in the natural order\n",
    "\n",
    "We can also create quickly the uniform capacity of any size:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "b0085ad7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.25,\n",
       " 0.25,\n",
       " 0.5,\n",
       " 0.25,\n",
       " 0.5,\n",
       " 0.5,\n",
       " 0.75,\n",
       " 0.25,\n",
       " 0.5,\n",
       " 0.5,\n",
       " 0.75,\n",
       " 0.5,\n",
       " 0.75,\n",
       " 0.75,\n",
       " 1.0]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "u_capacity = uniform_capacity(4)\n",
    "u_capacity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "055bbc8a",
   "metadata": {},
   "source": [
    "## Check function type constraints\n",
    "\n",
    "We can verify this set function is indeed a capacity directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "75c58c7c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_normal_capacity(capacity)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e77d441",
   "metadata": {},
   "source": [
    "We could also check all the other constraints to have a better understanding of the capacity we defined. Some are implicit, any capacity is a game and obviously a set function, but others are more peculiar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "ff0e25ed",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_powerset_function(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3fa95f82",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_game(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "d5982d12",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_capacity(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "9f0a569a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_additive(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "9d9e2ff4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_cardinality_based(capacity)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7f0c42d",
   "metadata": {},
   "source": [
    "We can use those to verify the uniform capacity is indeed uniform (additive, cardinality-based, normal):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "da5fa5a3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_normal_capacity(u_capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "72ad1eb6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_additive(u_capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "ae1f6580",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_cardinality_based(u_capacity)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3045069",
   "metadata": {},
   "source": [
    "## Analysis of aggregation\n",
    "\n",
    "This package implements the following metrics (for regular and möbius representation):\n",
    "* shapley values\n",
    "* interaction indexes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "c3e2598a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.1346153846153846,\n",
       " 0.21153846153846154,\n",
       " 0.28846153846153844,\n",
       " 0.3653846153846154]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "shapley_capacity(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "252819cb",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[nan, -0.025641025641025616, -0.025641025641025664, -0.025641025641025633],\n",
       " [-0.025641025641025616, nan, -0.06410256410256412, -0.06410256410256408],\n",
       " [-0.025641025641025664, -0.06410256410256412, nan, -0.06410256410256411],\n",
       " [-0.025641025641025647, -0.0641025641025641, -0.06410256410256412, nan]]"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "interaction_index_capacity(capacity)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77d0f88b",
   "metadata": {},
   "source": [
    "## Transformations\n",
    "\n",
    "As of today, the package only implements the Möbius transformation and its inverse.\n",
    "Multiple implementation of the Möbius transformation are currently proposed:\n",
    "* default Möbius transformation\n",
    "* fast Möbius transformation (from R kappalab package)\n",
    "* Möbius transformation using numpy arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "b521e604",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.07692307692307693,\n",
       " 0.15384615384615385,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " -0.15384615384615385,\n",
       " 0.3076923076923077,\n",
       " 0.1538461538461538,\n",
       " 0.23076923076923073,\n",
       " -0.15384615384615374,\n",
       " 0.23076923076923078,\n",
       " -0.15384615384615374,\n",
       " -0.3846153846153846,\n",
       " -0.0769230769230771]"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mobius = mobius_transform(capacity)\n",
    "mobius"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "3ac7cc0d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.07692307692307693,\n",
       " 0.15384615384615385,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " -0.1538461538461539,\n",
       " 0.3076923076923077,\n",
       " 0.1538461538461538,\n",
       " 0.23076923076923073,\n",
       " -0.1538461538461537,\n",
       " 0.23076923076923084,\n",
       " -0.1538461538461539,\n",
       " -0.3846153846153847,\n",
       " -0.07692307692307693]"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mobius_transform_fast(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "98934532",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.07692307692307693,\n",
       " 0.15384615384615385,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " 0.15384615384615385,\n",
       " 0.23076923076923078,\n",
       " -0.1538461538461539,\n",
       " 0.3076923076923077,\n",
       " 0.1538461538461538,\n",
       " 0.23076923076923073,\n",
       " -0.1538461538461537,\n",
       " 0.23076923076923084,\n",
       " -0.1538461538461539,\n",
       " -0.3846153846153847,\n",
       " -0.07692307692307693]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mobius_transform_numpy(capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "4760bfbf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.0,\n",
       " 0.07692307692307693,\n",
       " 0.15384615384615385,\n",
       " 0.38461538461538464,\n",
       " 0.23076923076923078,\n",
       " 0.46153846153846156,\n",
       " 0.6153846153846154,\n",
       " 0.8461538461538461,\n",
       " 0.3076923076923077,\n",
       " 0.5384615384615384,\n",
       " 0.6923076923076923,\n",
       " 0.9230769230769229,\n",
       " 0.7692307692307694,\n",
       " 1.0000000000000002,\n",
       " 1.0,\n",
       " 1.0]"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "inverse_mobius_transform(mobius)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7489c632",
   "metadata": {},
   "source": [
    "We can quickly look at the accuracy of both transformation and its inverse:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "b72f0695",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0 -> 0.0\n",
      "0.07692307692307693 -> 0.07692307692307693\n",
      "0.15384615384615385 -> 0.15384615384615385\n",
      "0.38461538461538464 -> 0.38461538461538464\n",
      "0.23076923076923078 -> 0.23076923076923078\n",
      "0.46153846153846156 -> 0.46153846153846156\n",
      "0.6153846153846154 -> 0.6153846153846154\n",
      "0.8461538461538461 -> 0.8461538461538461\n",
      "0.3076923076923077 -> 0.3076923076923077\n",
      "0.5384615384615384 -> 0.5384615384615384\n",
      "0.6923076923076923 -> 0.6923076923076923\n",
      "0.9230769230769231 -> 0.9230769230769229\n",
      "0.7692307692307693 -> 0.7692307692307694\n",
      "1.0 -> 1.0000000000000002\n",
      "1 -> 1.0\n",
      "1 -> 1.0\n"
     ]
    }
   ],
   "source": [
    "c = inverse_mobius_transform(mobius)\n",
    "for c1, c2 in zip(capacity, c):\n",
    "    print(f\"{c1} -> {c2}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "299db8e3",
   "metadata": {},
   "source": [
    "We can use this representation to compute the aggregation analysis metrics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "b8a4c565",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0.13461538461538464,\n",
       " 0.2115384615384615,\n",
       " 0.2884615384615385,\n",
       " 0.3653846153846154]"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "shapley_mobius(mobius)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "076d2367",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[nan, -0.025641025641025647, -0.025641025641025647, -0.025641025641025644],\n",
       " [-0.025641025641025647, nan, -0.06410256410256415, -0.06410256410256414],\n",
       " [-0.025641025641025647, -0.06410256410256415, nan, -0.06410256410256408],\n",
       " [-0.025641025641025644, -0.06410256410256414, -0.06410256410256408, nan]]"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "interaction_index_mobius(mobius)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae45d9a3",
   "metadata": {},
   "source": [
    "This representation also has constraints that define the type of its underlying set function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "0b7161f8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_mobius_capacity(mobius)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "a31cfc4c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_normal_capacity_mobius(mobius)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "f8341c6c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_k_additive(mobius, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "9ad6beb4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_k_additive(u_capacity, 1)  # equivalent to additivity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "13a15869",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "is_k_additive(u_capacity, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60a8339e",
   "metadata": {},
   "source": [
    "# Tests\n",
    "\n",
    "The following are comparisons made between the 3 Möbius transformation implementation in terms of accuracy and rapidity.\n",
    "The accuracy is tested by evaluating if the Möbius transform of a normal capacity is strictly normal (sum exactly equal to 1) for 100 random normal capacities per cardinality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "e503d838",
   "metadata": {},
   "outputs": [],
   "source": [
    "do_tests = False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "fdba2c55",
   "metadata": {},
   "outputs": [],
   "source": [
    "import random, time\n",
    "\n",
    "if do_tests:\n",
    "    for n in range(1, 9):\n",
    "        count = 0\n",
    "        t0 = time.time()\n",
    "        for r in range(100):\n",
    "            c = [0] + [random.random() for _ in range(2**n - 2)] + [1.0]\n",
    "            m = mobius_transform(c)\n",
    "            count += 1 if sum(m) != 1 else 0\n",
    "        t = time.time() - t0\n",
    "        print(f\"{n}: correct {100*(count/100)}% (took {t}s)\")\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "cdfec84b",
   "metadata": {},
   "outputs": [],
   "source": [
    "if do_tests:\n",
    "    for n in range(1, 9):\n",
    "        count = 0\n",
    "        t0 = time.time()\n",
    "        for r in range(100):\n",
    "            c = [0] + [random.random() for _ in range(2**n - 2)] + [1.0]\n",
    "            m = mobius_transform_fast(c)\n",
    "            count += 1 if sum(m) != 1 else 0\n",
    "        t = time.time() - t0\n",
    "        print(f\"{n}: correct {100*(count/100)}% (took {t}s)\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "340fb07c",
   "metadata": {},
   "outputs": [],
   "source": [
    "if do_tests:\n",
    "    for n in range(1, 9):\n",
    "        count = 0\n",
    "        t0 = time.time()\n",
    "        for r in range(100):\n",
    "            c = [0] + [random.random() for _ in range(2**n - 2)] + [1.0]\n",
    "            m = mobius_transform_numpy(c)\n",
    "            count += 1 if sum(m) != 1 else 0\n",
    "        t = time.time() - t0\n",
    "        print(f\"{n}: correct {100*(count/100)}% (took {t}s)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca332407",
   "metadata": {},
   "source": [
    "# Reference\n",
    "\n",
    "The example used in this notebook corresponds to the R package kappalab example, taken from its [documentation](https://cran.r-project.org/web/packages/kappalab/kappalab.pdf) at page 15. Below the R code corresponding:\n",
    "\n",
    "```R\n",
    "# load library 'kappalab'\n",
    "library(\"kappalab\")\n",
    "# a normalized capacity\n",
    "mu <- capacity(c(0:13/13,1,1))\n",
    "# compute mobius transform of capacity\n",
    "m <- Mobius(mu)\n",
    "# The Shapley values\n",
    "Shapley.value(mu)\n",
    "# The interaction index matrix\n",
    "interaction.indices(mu)\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
