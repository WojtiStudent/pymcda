import unittest
from math import log

from pandas import DataFrame

from mcda.core.functions import FuzzyNumber
from mcda.core.scales import (
    FuzzyScale,
    PreferenceDirection,
    QualitativeScale,
    QuantitativeScale,
)
from mcda.mavt.aggregators import (
    choquet_integral_capacity,
    choquet_integral_mobius,
    normalized_weighted_sum,
    owa,
    owa_and_weights,
    owa_or_weights,
    owa_weights_andness,
    owa_weights_balance,
    owa_weights_dispersion,
    owa_weights_divergence,
    owa_weights_orness,
    owa_weights_quantifier,
    quantifier_to_owa_weights,
    ulowa,
    ulowa_delta,
    ulowa_most_similar,
    weighted_sum,
)


def test_weighted_sum_after_normalization():
    table = DataFrame([[0, 1, 2, 3], [4, 5, 6, 7]])
    weights = {i: 1 for i in range(4)}
    res = normalized_weighted_sum(table, weights)
    assert len(res) == 2
    assert res[0] == 6 and res[1] == 22


def test_weighted_sum():
    scales = {
        0: QuantitativeScale(0, 10),
        1: QualitativeScale(
            ["bad", "medium", "good"], [3, 2, 1], PreferenceDirection.MIN
        ),
        2: QuantitativeScale(0, 1, PreferenceDirection.MIN),
    }
    table = DataFrame([[0, "good", 0.5], [4, "medium", 0]])
    weights = [1, 2, 1]
    res = weighted_sum(table, scales, weights)
    assert len(res) == 2
    assert res[0] == 2.5 and res[1] == 2.4


def test_owa():
    values = [0.6, 1.0, 0.3, 0.5]
    weights = [0.2, 0.3, 0.1, 0.4]
    assert owa(values, weights) == 0.55


def test_owa_and_weights():
    weights = owa_and_weights(5)
    assert len(weights) == 5
    for w in weights[:-1]:
        assert w == 0
    assert weights[-1] == 1


def test_owa_or_weights():
    weights = owa_or_weights(5)
    assert len(weights) == 5
    for w in weights[1:]:
        assert w == 0
    assert weights[0] == 1


def test_owa_weights_orness():
    assert owa_weights_orness([1, 0, 0, 0, 0]) == 1
    assert owa_weights_orness([0, 0, 0, 1]) == 0
    assert owa_weights_orness([1 / 4, 1 / 4, 1 / 4, 1 / 4]) == 0.5


def test_owa_weights_andness():
    assert owa_weights_andness([1, 0, 0, 0, 0]) == 0
    assert owa_weights_andness([0, 0, 0, 1]) == 1
    assert owa_weights_andness([1 / 4, 1 / 4, 1 / 4, 1 / 4]) == 0.5


def test_owa_weights_dispersion():
    assert owa_weights_dispersion([0, 1, 0, 0]) == 0
    assert owa_weights_dispersion([1 / 4, 1 / 4, 1 / 4, 1 / 4]) == log(4)


def test_owa_weights_balance():
    assert owa_weights_balance([1, 0, 0, 0, 0]) == 1
    assert owa_weights_balance([0, 0, 0, 1]) == -1
    assert owa_weights_balance([1 / 4, 1 / 4, 1 / 4, 1 / 4]) == 0


def test_owa_weights_divergence():
    assert owa_weights_divergence([1 / 2, 0, 0, 1 / 2]) == 0.25
    assert owa_weights_divergence([0, 0, 1 / 2, 0, 1 / 2, 0, 0]) == 1 / 36


def test_owa_weights_quantifier():
    quantifier = owa_weights_quantifier([0, 0, 1])
    print(quantifier)
    assert len(quantifier) == 4
    for q in quantifier[:-1]:
        assert q == 0
    assert quantifier[-1] == 1
    quantifier = owa_weights_quantifier([1, 0, 0])
    assert quantifier[0] == 0
    for q in quantifier[1:]:
        assert q == 1


def test_quantifier_to_owa_weights():
    assert quantifier_to_owa_weights(owa_weights_quantifier([0, 1, 1])) == [
        0,
        1,
        1,
    ]


class TestUlowa(unittest.TestCase):
    def setUp(self):
        self.fuzzy_sets = [
            FuzzyNumber([0, 0, 0, 0]),
            FuzzyNumber([0.0, 0.0, 0.0, 2.0]),
            FuzzyNumber([0.0, 2.0, 2.0, 5.0]),
            FuzzyNumber([2.0, 5.0, 5.0, 6.0]),
            FuzzyNumber([5.0, 6.0, 6.0, 7.0]),
            FuzzyNumber([6.0, 7.0, 8.0, 9.0]),
            FuzzyNumber([8.0, 9.0, 9.0, 10.0]),
            FuzzyNumber([9.0, 10.0, 10.0, 10.0]),
            FuzzyNumber([10, 10, 10, 10]),
        ]
        self.labels = ["N", "VL", "L", "M", "AH", "H", "VH", "P", "PP"]
        self.scale = FuzzyScale(self.labels, self.fuzzy_sets)
        self.weights = [0.0, 0.0, 0.5, 0.5, 0.0]
        self.performance_table = [
            ["VL", "VL", "P", "H", "VL"],
            ["VL", "VL", "H", "P", "P"],
            ["VL", "VL", "L", "M", "L"],
            ["VH", "L", "H", "H", "AH"],
            ["P", "L", "H", "L", "AH"],
            ["P", "VL", "VL", "M", "AH"],
        ]

    def test_ulowa_delta(self):
        self.assertEqual(ulowa_delta("N", "PP", 0.5, self.scale), 5.0)

    def test_ulowa_most_similar(self):
        self.assertEqual(
            ulowa_most_similar(
                "VL", "H", FuzzyNumber([5.5, 6.1, 6.5, 7.5]), self.scale
            ),
            "AH",
        )
        self.assertEqual(
            ulowa_most_similar(
                "VL", "M", FuzzyNumber([5.5, 6.1, 6.5, 7.5]), self.scale
            ),
            "M",
        )

    def test_ulowa(self):
        results = [
            ulowa(aperf, self.weights, self.scale)
            for aperf in self.performance_table
        ]
        previsions = ["VL", "M", "VL", "AH", "M", "L"]
        self.assertEqual(results, previsions)
        self.assertRaises(ValueError, ulowa, [], self.weights, self.scale)


class TestChoquet(unittest.TestCase):
    def setUp(self):
        # Value taken from R kappalab package (7 digits accuracy)
        self.capacity = [
            0.0,
            0.07692307692307693,
            0.15384615384615385,
            0.38461538461538464,
            0.23076923076923078,
            0.46153846153846156,
            0.6153846153846154,
            0.8461538461538461,
            0.3076923076923077,
            0.5384615384615384,
            0.6923076923076923,
            0.9230769230769231,
            0.7692307692307693,
            1.0,
            1,
            1,
        ]
        self.mobius = [
            0,
            0.07692308,
            0.15384615,
            0.15384615,
            0.23076923,
            0.15384615,
            0.23076923,
            -0.15384615,
            0.30769231,
            0.15384615,
            0.23076923,
            -0.15384615,
            0.23076923,
            -0.15384615,
            -0.38461538,
            -0.07692308,
        ]
        self.values = [0.1, 0.9, 0.3, 0.8]
        self.choquet = 0.6615385

    def test_choquet_integral_capacity(self):
        self.assertAlmostEqual(
            choquet_integral_capacity(self.values, self.capacity), self.choquet
        )

    def test_choquet_integral_mobius(self):
        self.assertAlmostEqual(
            choquet_integral_mobius(self.values, self.mobius), self.choquet
        )
