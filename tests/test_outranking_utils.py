from pandas import DataFrame
from pandas.testing import assert_frame_equal

from mcda.outranking.utils import cycle_reduction_matrix, dijskstra


def test_dijkstra():
    track = [0, 5]
    graph = {0: [1, 5], 1: [], 2: [], 3: [1, 5], 4: [0, 1, 2, 3, 5], 5: []}
    calculated_dijkstra_tracks = dijskstra(graph, 4, 5)
    for i in range(len(track)):
        assert track[i] == calculated_dijkstra_tracks[i]


def test_cycle_reduction_matrix():
    matrix = DataFrame(
        [
            [1, 1, 0, 1, 1, 0],
            [0, 1, 1, 1, 1, 0],
            [0, 1, 0, 1, 1, 0],
            [0, 0, 0, 1, 1, 0],
            [0, 0, 0, 1, 0, 1],
            [0, 0, 0, 1, 0, 0],
        ]
    )
    expected_matrix = DataFrame(
        [
            [0, 1, 1, 1, 1, 1],
            [0, 0, 0, 1, 1, 1],
            [0, 0, 0, 1, 1, 1],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
        ]
    )
    results = cycle_reduction_matrix(matrix)
    assert_frame_equal(expected_matrix, results)
