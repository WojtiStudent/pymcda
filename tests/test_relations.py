from unittest import TestCase

from pandas import DataFrame, Series
from pandas._testing import assert_frame_equal

from mcda.core.relations import (
    RelationType,
    add_transitivity,
    alternative_relations,
    compatible,
    elements,
    equal,
    in_relations,
    is_preference_structure,
    is_total_order,
    is_total_preorder,
    matrix_to_relations,
    relation,
    relations_pair_unicity,
    relations_to_matrix,
    relations_typed,
    remove_transitivity,
    same_alternatives,
    scores_to_relations,
    transitive_closure,
    transitive_reduction_matrix,
    valid,
)


def test_relation_types():
    RelationType.has_value(RelationType.PREFERENCE)
    assert RelationType.PREFERENCE != RelationType.INDIFFERENCE
    s = RelationType.content_message()
    assert len(s) > 0


def test_scores_to_relations():
    scores = Series({0: 2.0, 1: 2.0, 2: 1.8, 3: 1.5, 4: 1.5, 5: -0.8})
    expected = [
        [(1, 0, RelationType.INDIFFERENCE), (0, 1, RelationType.INDIFFERENCE)],
        [(0, 2, RelationType.PREFERENCE), (1, 2, RelationType.PREFERENCE)],
        [(2, 4, RelationType.PREFERENCE)],
        [(4, 3, RelationType.INDIFFERENCE), (3, 4, RelationType.INDIFFERENCE)],
        [(3, 5, RelationType.PREFERENCE), (4, 5, RelationType.PREFERENCE)],
    ]
    relations = scores_to_relations(scores)
    assert len(relations) == len(scores) - 1
    for r, e in zip(relations, expected):
        assert r in e

    expected_rev = [
        [(5, 3, RelationType.PREFERENCE), (5, 4, RelationType.PREFERENCE)],
        [(4, 3, RelationType.INDIFFERENCE), (3, 4, RelationType.INDIFFERENCE)],
        [(4, 2, RelationType.PREFERENCE)],
        [(2, 0, RelationType.PREFERENCE), (2, 1, RelationType.PREFERENCE)],
        [(1, 0, RelationType.INDIFFERENCE), (0, 1, RelationType.INDIFFERENCE)],
    ]
    for r, e in zip(scores_to_relations(scores, ascending=True), expected_rev):
        assert r in e


class TestRelations(TestCase):
    def setUp(self):
        self.alternatives = [0, 1, 2, 3, 4, 5, 6]
        self.relations = [
            (1, 0, RelationType.INDIFFERENCE),
            (0, 2, RelationType.PREFERENCE),
            (2, 4, RelationType.PREFERENCE),
            (4, 3, RelationType.INDIFFERENCE),
            (4, 5, RelationType.PREFERENCE),
            (5, 6, RelationType.INCOMPARABLE),
        ]
        self.all_relations = [
            (1, 1, RelationType.INDIFFERENCE),
            (1, 0, RelationType.INDIFFERENCE),
            (0, 0, RelationType.INDIFFERENCE),
            (0, 2, RelationType.PREFERENCE),
            (2, 4, RelationType.PREFERENCE),
            (4, 3, RelationType.INDIFFERENCE),
            (3, 4, RelationType.INDIFFERENCE),
            (4, 5, RelationType.PREFERENCE),
            (0, 2, RelationType.PREFERENCE),
            (5, 6, RelationType.INCOMPARABLE),
            (6, 5, RelationType.INCOMPARABLE),
        ]

    def test_valid(self):
        self.assertTrue(valid((0, 0, RelationType.INDIFFERENCE)))
        self.assertTrue(valid((0, 1, RelationType.PREFERENCE)))
        self.assertFalse(valid((0, 0, RelationType.INCOMPARABLE)))
        self.assertFalse(valid((0, 0, RelationType.PREFERENCE)))

    def test_same_alternatives(self):
        self.assertTrue(
            same_alternatives(self.relations[0], self.relations[0])
        )
        self.assertTrue(
            same_alternatives(
                (0, 1, RelationType.INDIFFERENCE),
                (1, 0, RelationType.INDIFFERENCE),
            )
        )

    def test_equal(self):
        self.assertTrue(
            equal(
                (0, 1, RelationType.PREFERENCE),
                (0, 1, RelationType.PREFERENCE),
            )
        )
        self.assertFalse(
            equal(
                (0, 1, RelationType.PREFERENCE),
                (0, 2, RelationType.PREFERENCE),
            )
        )
        self.assertFalse(
            equal(
                (0, 1, RelationType.PREFERENCE),
                (0, 1, RelationType.INDIFFERENCE),
            )
        )
        self.assertFalse(
            equal(
                (0, 1, RelationType.PREFERENCE),
                (1, 0, RelationType.PREFERENCE),
            )
        )
        self.assertTrue(
            equal(
                (0, 1, RelationType.INDIFFERENCE),
                (1, 0, RelationType.INDIFFERENCE),
            ),
        )
        self.assertTrue(
            equal(
                (0, 1, RelationType.INCOMPARABLE),
                (1, 0, RelationType.INCOMPARABLE),
            ),
        )

    def test_compatible(self):
        self.assertTrue(
            compatible(
                (0, 1, RelationType.PREFERENCE),
                (0, 1, RelationType.PREFERENCE),
            )
        )
        self.assertTrue(
            compatible(
                (0, 1, RelationType.PREFERENCE),
                (0, 2, RelationType.PREFERENCE),
            )
        )
        self.assertFalse(
            compatible(
                (0, 1, RelationType.PREFERENCE),
                (0, 1, RelationType.INDIFFERENCE),
            )
        )
        self.assertTrue(
            compatible(
                (0, 1, RelationType.INDIFFERENCE),
                (1, 0, RelationType.INDIFFERENCE),
            )
        )
        self.assertTrue(
            compatible(
                (0, 1, RelationType.INCOMPARABLE),
                (1, 0, RelationType.INCOMPARABLE),
            )
        )
        self.assertFalse(
            compatible(
                (0, 1, RelationType.PREFERENCE),
                (1, 0, RelationType.PREFERENCE),
            )
        )

    def test_in_relations(self):
        self.assertTrue(
            in_relations((1, 0, RelationType.INDIFFERENCE), self.relations)
        )
        self.assertTrue(
            in_relations((0, 1, RelationType.INDIFFERENCE), self.relations)
        )
        self.assertFalse(
            in_relations((2, 0, RelationType.PREFERENCE), self.relations)
        )

    def test_elements(self):
        self.assertEqual(set(self.alternatives), set(elements(self.relations)))

    def test_relation(self):
        self.assertEqual(relation(1, 0, self.relations), self.relations[0])
        self.assertEqual(relation(0, 1, self.relations), self.relations[0])
        self.assertIsNone(relation(2, 5, self.relations))

    def test_alternative_relations(self):
        res = alternative_relations(0, self.relations)
        expected = [
            (1, 0, RelationType.INDIFFERENCE),
            (0, 2, RelationType.PREFERENCE),
        ]
        self.assertEqual(set(res), set(expected))

    def test_relations_typed(self):
        prefs = relations_typed(RelationType.PREFERENCE, self.relations)
        indifs = relations_typed(RelationType.INDIFFERENCE, self.relations)
        incomps = relations_typed(RelationType.INCOMPARABLE, self.relations)
        for _, _, r in prefs:
            self.assertEqual(r, RelationType.PREFERENCE)
        for _, _, r in indifs:
            self.assertEqual(r, RelationType.INDIFFERENCE)
        for _, _, r in incomps:
            self.assertEqual(r, RelationType.INCOMPARABLE)
        res = prefs + indifs + incomps
        self.assertEqual(set(res), set(self.relations))

    def test_relations_pair_unicity(self):
        self.assertTrue(relations_pair_unicity(self.relations))
        self.assertFalse(
            relations_pair_unicity(
                self.relations + [(4, 2, RelationType.INDIFFERENCE)]
            )
        )
        self.assertFalse(
            relations_pair_unicity(self.relations + [self.relations[0]])
        )

    def test_is_preference_structure(self):
        self.assertTrue(is_preference_structure(self.relations))
        self.assertTrue(is_preference_structure(self.all_relations))
        self.assertFalse(
            is_preference_structure(
                self.relations + [(5, 5, RelationType.INCOMPARABLE)]
            )
        )
        self.assertFalse(
            is_preference_structure(
                self.relations + [(2, 0, RelationType.PREFERENCE)]
            )
        )

    def test_is_total_preorder(self):
        self.assertFalse(is_total_preorder(self.relations))
        self.assertTrue(is_total_preorder(self.relations[:-1]))
        self.assertFalse(
            is_total_preorder(
                self.relations + [(5, 6, RelationType.INDIFFERENCE)]
            )
        )

    def test_is_total_order(self):
        self.assertFalse(is_total_order(self.relations))
        self.assertTrue(
            is_total_order(
                [
                    (0, 1, RelationType.PREFERENCE),
                    (1, 2, RelationType.PREFERENCE),
                    (2, 3, RelationType.PREFERENCE),
                    (3, 4, RelationType.PREFERENCE),
                ]
            )
        )
        self.assertFalse(
            is_total_order(
                self.relations + [(5, 6, RelationType.INDIFFERENCE)]
            )
        )

    def test_transitive_closure(self):
        res = transitive_closure(relations_to_matrix(self.relations))
        print(res)
        expected = DataFrame(
            [
                [1, 1, 1, 1, 1, 1, 0],
                [1, 1, 1, 1, 1, 1, 0],
                [0, 0, 1, 1, 1, 1, 0],
                [0, 0, 0, 1, 1, 1, 0],
                [0, 0, 0, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 1, 0],
                [0, 0, 0, 0, 0, 0, 1],
            ]
        )
        self.assertTrue((res == expected).all(None))

    def test_add_transitivity(self):
        res = add_transitivity(self.relations)
        self.assertTrue(
            set(res),
            set(
                matrix_to_relations(
                    transitive_closure(relations_to_matrix(self.relations))
                ),
            ),
        )

    def test_remove_transitivity(self):
        res = remove_transitivity(self.relations)
        self.assertEqual(
            set(res),
            set(
                matrix_to_relations(
                    transitive_reduction_matrix(
                        relations_to_matrix(self.relations)
                    )
                ),
            ),
        )


def test_matrix_to_relation_list():
    matrix = DataFrame(
        [[0, 1, 1, 0], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]
    )
    expected_relations = [
        (0, 1, RelationType.PREFERENCE),
        (0, 2, RelationType.PREFERENCE),
        (3, 0, RelationType.PREFERENCE),
        (1, 2, RelationType.INDIFFERENCE),
        (1, 3, RelationType.INCOMPARABLE),
        (2, 3, RelationType.INCOMPARABLE),
    ]
    relations = matrix_to_relations(matrix)
    for i in range(len(relations)):
        assert (
            expected_relations[i][:-1] == relations[i][:-1]
            and expected_relations[i][2] == relations[i][2]
        )


def test_relation_to_matrix():
    expected_matrix = DataFrame(
        [
            [1, 1, 1, 1],
            [0, 1, 1, 0],
            [0, 1, 1, 0],
            [0, 0, 0, 1],
        ]
    )
    relations = [
        (0, 1, RelationType.PREFERENCE),
        (0, 2, RelationType.PREFERENCE),
        (0, 3, RelationType.PREFERENCE),
        (1, 2, RelationType.INDIFFERENCE),
        (2, 1, RelationType.INDIFFERENCE),
    ]
    matrix = relations_to_matrix(relations)
    assert_frame_equal(expected_matrix, matrix)


def test_transitive_reduction_matrix():
    matrix = DataFrame(
        [[0, 1, 1, 1], [0, 0, 1, 1], [0, 0, 0, 1], [0, 0, 0, 0]]
    )
    expected_matrix = DataFrame(
        [[0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0]]
    )
    results = transitive_reduction_matrix(matrix)
    assert_frame_equal(expected_matrix, results)

    matrix = DataFrame(
        [[0, 0, 0, 0], [1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 0]]
    )
    expected_matrix = DataFrame(
        [[0, 0, 0, 0], [1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]]
    )
    results = transitive_reduction_matrix(matrix)
    assert_frame_equal(expected_matrix, results)
