import numpy as np
import pytest

from mcda.utils import (
    PolymorphicFunction,
    VectorizedFunction,
    is_unique_sequence,
)


def test_is_unique_sequence():
    assert is_unique_sequence([12, "a", None])
    assert is_unique_sequence((12, "12"))
    assert not is_unique_sequence(["a", 1, True, "a"])


def test_polymorphic_functions():
    functions = {int: lambda x: 2 * x, list: lambda x: [2 * xx for xx in x]}
    f = PolymorphicFunction(functions)
    assert f(10) == 20
    with pytest.raises(Exception) as e:
        f(1.5)
    assert e.type == TypeError
    res = f([0, 1, 2])
    assert res[0] == 0 and res[1] == 2 and res[2] == 4
    assert len(f._types()) == 2

    f = PolymorphicFunction(default=lambda x: 2 * x)
    assert f(1.5) == 3


def test_vectorized_functions():
    # Test constructor
    f = VectorizedFunction(function=lambda x: 2 * x)
    assert f(2) == 4 and f(0.6) == 1.2
    res = f([10, 0.5, -3.1])
    assert res[0] == 20 and res[1] == 1 and res[2] == -6.2
    res = f({"a": 5, "12": 0})
    assert res["a"] == 10 and res["12"] == 0
    res = f(np.array([0.1, -2.5]))
    assert res[0] == 0.2 and res[1] == -5

    functions = {
        int: lambda x: 2 * x,
        list: lambda x: [2 * xx for xx in x],
        dict: lambda x: {k: 2 * v for k, v in x.items()},
        np.ndarray: lambda x: 2 * x,
    }
    f = VectorizedFunction(functions=functions, function=lambda x: 2 * x)
    assert f(2) == 4 and f(0.6) == 1.2
    res = f([10, 0.5, -3.1])
    assert res[0] == 20 and res[1] == 1 and res[2] == -6.2
