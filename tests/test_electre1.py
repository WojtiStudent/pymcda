import pytest
from pandas import DataFrame
from pandas.testing import assert_frame_equal

from mcda.core.scales import PreferenceDirection, QuantitativeScale
from mcda.outranking.electre1 import (
    concordance,
    discordance,
    electre1,
    outranking,
)


@pytest.fixture
def dataset():
    return DataFrame(
        [
            [4, 2, 1, 5, 2, 2, 4],
            [3, 5, 3, 5, 3, 3, 3],
            [3, 5, 3, 5, 3, 2, 2],
            [4, 2, 2, 5, 1, 1, 1],
            [4, 1, 3, 5, 4, 1, 5],
        ]
    )


@pytest.fixture
def weight():
    return {
        0: 0.780,
        1: 1.180,
        2: 1.570,
        3: 3.140,
        4: 2.350,
        5: 0.390,
        6: 0.590,
    }


@pytest.fixture
def scales():
    return {
        0: QuantitativeScale(1, 5, PreferenceDirection.MIN),
        1: QuantitativeScale(1, 5),
        2: QuantitativeScale(1, 5),
        3: QuantitativeScale(1, 5),
        4: QuantitativeScale(1, 5),
        5: QuantitativeScale(1, 5),
        6: QuantitativeScale(1, 5),
    }


def test_concordance_matrix(dataset, weight, scales):
    concordance_matrix = DataFrame(
        [
            [1.0, 0.37, 0.41, 0.84, 0.55],
            [0.94, 1.0, 1.0, 1.0, 0.71],
            [0.94, 0.9, 1.0, 1.0, 0.71],
            [0.67, 0.31, 0.31, 1.0, 0.55],
            [0.84, 0.77, 0.77, 0.88, 1.0],
        ]
    )
    # Absolute value = 0.006 according to usual math rounding method
    # (if last digit >5 => round to superior else otherwise
    assert_frame_equal(
        concordance(dataset, weight, scales), concordance_matrix, atol=0.006
    )


def test_discordance_matrix(dataset, scales):
    discordance_matrix = DataFrame(
        [
            [0.0, 0.75, 0.75, 0.25, 0.5],
            [0.25, 0.0, 0.0, 0.0, 0.5],
            [0.5, 0.25, 0.0, 0.0, 0.75],
            [0.75, 0.75, 0.75, 0.0, 1.0],
            [0.25, 1.0, 1.0, 0.25, 0.0],
        ]
    )

    assert_frame_equal(discordance(dataset, scales), discordance_matrix)


def test_outranking_matrix(dataset, weight, scales):
    outranking_matrix = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 0],
            [1, 1, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [1, 0, 0, 1, 1],
        ]
    )
    c_hat = 0.75
    d_hat = 0.50
    assert_frame_equal(
        outranking(
            concordance(dataset, weight, scales),
            discordance(dataset, scales),
            c_hat,
            d_hat,
        ),
        outranking_matrix,
        check_dtype=False,
    )


def test_electre1_matrix(dataset, weight, scales):
    outranking_matrix = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 0],
            [1, 1, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [1, 0, 0, 1, 1],
        ]
    )
    c_hat = 0.75
    d_hat = 0.50

    # Absolute value = 0.006 according to usual math rounding method
    # (if last digit >5 => round to superior else otherwise
    assert_frame_equal(
        electre1(dataset, weight, scales, c_hat, d_hat),
        outranking_matrix,
        check_dtype=False,
    )

    concordance_matrix = DataFrame(
        [
            [1.0, 0.37, 0.41, 0.84, 0.55],
            [0.94, 1.0, 1.0, 1.0, 0.71],
            [0.94, 0.9, 1.0, 1.0, 0.71],
            [0.67, 0.31, 0.31, 1.0, 0.55],
            [0.84, 0.77, 0.77, 0.88, 1.0],
        ]
    )
    discordance_matrix = DataFrame(
        [
            [0.0, 0.75, 0.75, 0.25, 0.5],
            [0.25, 0.0, 0.0, 0.0, 0.5],
            [0.5, 0.25, 0.0, 0.0, 0.75],
            [0.75, 0.75, 0.75, 0.0, 1.0],
            [0.25, 1.0, 1.0, 0.25, 0.0],
        ]
    )

    # Absolute value = 0.006 according to usual math rounding method
    # (if last digit >5 => round to superior else otherwise
    assert_frame_equal(
        electre1(
            dataset,
            weight,
            scales,
            c_hat,
            d_hat,
            concordance_matrix=concordance_matrix,
            discordance_matrix=discordance_matrix,
        ),
        outranking_matrix,
        check_dtype=False,
    )
