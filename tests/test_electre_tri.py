import pytest
from pandas import DataFrame

from mcda.core.scales import QuantitativeScale
from mcda.outranking.electre_tri import electre_tri, exploitation_procedure


@pytest.fixture
def dataset():
    return DataFrame(
        [
            [0.720, 3.560, 1.340, 0.62, 44.340],
            [0.8, 3.940, 1.430, 0.74, 36.360],
            [0.760, 3.630, 1.380, 0.89, 48.750],
            [0.780, 3.740, 1.450, 0.72, 42.130],
            [0.740, 3.540, 1.370, 0.73, 36.990],
            [0.690, 3.740, 1.450, 0.84, 42.430],
            [0.7, 3.280, 1.280, 0.83, 47.430],
            [0.860, 3.370, 1.150, 0.8, 80.790],
        ]
    )


@pytest.fixture
def weight():
    return {0: 30, 1: 30, 2: 20, 3: 10, 4: 10}


@pytest.fixture
def scales():
    return {
        0: QuantitativeScale(0, 1),
        1: QuantitativeScale(3, 4),
        2: QuantitativeScale(1, 2),
        3: QuantitativeScale(0, 1),
        4: QuantitativeScale(30, 100),
    }


@pytest.fixture
def preference():
    return {0: 0.05, 1: 0.1, 2: 0.05, 3: 0.1, 4: 8}


@pytest.fixture
def indifference():
    return {0: 0.02, 1: 0.05, 2: 0.02, 3: 0.05, 4: 2}


@pytest.fixture
def veto():
    return {0: 0.15, 1: 0.6, 2: 0.25, 3: 0.25, 4: 15}


@pytest.fixture
def profil():
    return DataFrame(
        [
            [0.750, 3.500, 1.300, 0.730, 42.00],
            [0.800, 3.700, 1.370, 0.790, 43.000],
        ]
    )


def test_procedure(
    dataset, weight, scales, preference, indifference, veto, profil
):
    lambda_ = 0.7
    pessimistic_classes = {
        0: [0, 4, 6],
        1: [2, 3, 5],
        2: [1],
        -1: [7],
    }
    optimistic_classes = {
        0: [6],
        1: [0, 2, 4],
        2: [1, 3, 5],
        -1: [7],
    }

    calculated_pessimistic_classes = exploitation_procedure(
        dataset,
        profil,
        weight,
        scales,
        preference,
        indifference,
        veto,
        lambda_,
        pessimistic=True,
    )
    calculated_optimistic_classes = exploitation_procedure(
        dataset,
        profil,
        weight,
        scales,
        preference,
        indifference,
        veto,
        lambda_,
    )

    assert pessimistic_classes == calculated_pessimistic_classes
    assert optimistic_classes == calculated_optimistic_classes


def test_electre_tri(
    dataset, weight, scales, preference, indifference, veto, profil
):
    lambda_ = 0.7
    (
        calculated_optimistic_classes,
        calculated_pessimistic_classes,
    ) = electre_tri(
        dataset,
        profil,
        weight,
        scales,
        preference,
        indifference,
        veto,
        lambda_,
    )
    optimistic_classes = {
        0: [6],
        1: [0, 2, 4],
        2: [1, 3, 5],
        -1: [7],
    }
    pessimistic_classes = {
        0: [0, 4, 6],
        1: [2, 3, 5],
        2: [1],
        -1: [7],
    }

    assert optimistic_classes == calculated_optimistic_classes
    assert pessimistic_classes == calculated_pessimistic_classes

    extreme_profil = DataFrame(
        [
            [0.650, 3.00, 1.200, 0.70, 42.00],
            [0.850, 4.00, 1.50, 0.90, 50.000],
        ]
    )

    extreme_dataset = DataFrame(
        [
            [0.720, 3.560, 1.340, 0.62, 44.340],
            [0.760, 3.630, 1.380, 0.89, 48.750],
            [0.780, 3.740, 1.450, 0.72, 42.130],
            [0.740, 3.540, 1.370, 0.73, 36.990],
            [0.690, 3.740, 1.450, 0.84, 42.430],
            [0.7, 3.280, 1.280, 0.83, 47.430],
        ]
    )
    (
        calculated_optimistic_classes,
        calculated_pessimistic_classes,
    ) = electre_tri(
        extreme_dataset,
        extreme_profil,
        weight,
        scales,
        preference,
        indifference,
        veto,
        lambda_,
    )
    optimistic_classes = {
        1: [0, 1, 2, 3, 4, 5],
    }
    pessimistic_classes = {
        1: [0, 1, 2, 3, 4, 5],
    }

    assert optimistic_classes == calculated_optimistic_classes
    assert pessimistic_classes == calculated_pessimistic_classes
