import unittest

from pandas import DataFrame
from pulp import LpMinimize, LpProblem

from mcda.core.performance_table import (
    apply_criteria_functions,
    get_alternative_values_at,
    normalize,
    sum_table,
)
from mcda.core.relations import RelationType
from mcda.core.scales import PreferenceDirection, QuantitativeScale
from mcda.mavt.uta import (
    add_uta_constraints,
    add_uta_star_constraints,
    generate_alternatives_errors_variables,
    generate_criteria_values_matrix,
    generate_marginal_utility_variables,
    generate_uta_problem,
    generate_uta_star_problem,
    generate_utility_variable,
    generate_utility_variable_star,
    normalized_uta,
    normalized_uta_star,
    post_optimality,
    star_post_optimality,
    uta,
    uta_star,
)


class UTATestCase(unittest.TestCase):
    def setUp(self):
        self.alternatives = [
            "Peugeot 505 GR",
            "Opel Record 2000 LS",
            "Citroen Visa Super E",
            "VW Golf 1300 GLS",
            "Citroen CX 2400 Pallas",
            "Mercedes 230",
            "BMW 520",
            "Volvo 244 DL",
            "Peugeot 104 ZS",
            "Citroen Dyane",
        ]
        self.performance_table = DataFrame(
            [
                [173, 11.4, 10.01, 10, 7.88, 49500],
                [176, 12.3, 10.48, 11, 7.96, 46700],
                [142, 8.2, 7.3, 5, 5.65, 32100],
                [148, 10.5, 9.61, 7, 6.15, 39150],
                [178, 14.5, 11.05, 13, 8.06, 64700],
                [180, 13.6, 10.4, 13, 8.47, 75700],
                [182, 12.7, 12.26, 11, 7.81, 68593],
                [145, 14.3, 12.95, 11, 8.38, 55000],
                [161, 8.6, 8.42, 7, 5.11, 35200],
                [117, 7.2, 6.75, 3, 5.81, 24800],
            ],
            index=self.alternatives,
        )
        self.criteria_segments = {0: 5, 1: 4, 2: 4, 3: 5, 4: 4, 5: 5}
        self.scales = {
            0: QuantitativeScale(110, 190),
            1: QuantitativeScale(7, 15, PreferenceDirection.MIN),
            2: QuantitativeScale(6, 13, PreferenceDirection.MIN),
            3: QuantitativeScale(3, 13),
            4: QuantitativeScale(5, 9),
            5: QuantitativeScale(20000, 80000, PreferenceDirection.MIN),
        }
        self.relations = [
            (
                self.alternatives[i],
                self.alternatives[i + 1],
                RelationType.PREFERENCE,
            )
            for i in range(len(self.alternatives) - 1)
        ]
        self.normalized_table = normalize(self.performance_table, self.scales)

    def test_generate_criteria_values_matrix(self):
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        self.assertEqual(len(g_matrix), len(self.criteria_segments))
        for i in g_matrix.keys():
            self.assertEqual(len(g_matrix[i]), self.criteria_segments[i] + 1)

    def test_generate_marginal_utility_variables(self):
        u_var = generate_marginal_utility_variables(self.criteria_segments)
        self.assertEqual(len(u_var), len(self.criteria_segments))
        for i in u_var.keys():
            self.assertEqual(len(u_var[i]), self.criteria_segments[i] + 1)

    def test_generate_alternatives_errors_variables(self):
        sigma_var = generate_alternatives_errors_variables(self.alternatives)
        self.assertEqual(len(sigma_var), len(self.alternatives))
        var = generate_alternatives_errors_variables(
            self.alternatives, "prefix"
        )
        for i, a in enumerate(self.alternatives):
            self.assertEqual(var[a].name, f"prefix_{i}")

    def test_generate_utility_variable(self):
        u_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        self.assertIsNotNone(
            generate_utility_variable(
                get_alternative_values_at(self.performance_table, 0),
                u_var,
                g_matrix,
            )
        )

    def test_generate_utility_variable_star(self):
        w_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        self.assertIsNotNone(
            generate_utility_variable_star(
                get_alternative_values_at(self.performance_table, 0),
                w_var,
                g_matrix,
            )
        )

    def test_add_uta_constraints(self):
        prob = LpProblem("UTA", LpMinimize)
        self.assertIsNone(prob.objective)
        u_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        sigma_var = generate_alternatives_errors_variables(self.alternatives)
        relations = [
            (
                self.alternatives[i],
                self.alternatives[i + 1],
                RelationType.PREFERENCE,
            )
            for i in range(len(self.alternatives) - 1)
        ]
        relations[2] = (
            self.alternatives[2],
            self.alternatives[3],
            RelationType.INDIFFERENCE,
        )
        add_uta_constraints(
            prob, self.normalized_table, u_var, sigma_var, g_matrix, relations
        )
        self.assertEqual(
            len(prob.constraints),
            len(relations) + sum(len(u_i) for u_i in u_var.values()) + 1,
        )

    def test_add_uta_star_constraints(self):
        prob = LpProblem("UTA_star", LpMinimize)
        self.assertIsNone(prob.objective)
        w_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        sigma_p_var = generate_alternatives_errors_variables(
            self.alternatives, "sigma_p"
        )
        sigma_n_var = generate_alternatives_errors_variables(
            self.alternatives, "sigma_n"
        )
        relations = [
            (
                self.alternatives[i],
                self.alternatives[i + 1],
                RelationType.PREFERENCE,
            )
            for i in range(len(self.alternatives) - 1)
        ]
        relations[2] = (
            self.alternatives[2],
            self.alternatives[3],
            RelationType.INDIFFERENCE,
        )
        add_uta_star_constraints(
            prob,
            self.normalized_table,
            w_var,
            sigma_p_var,
            sigma_n_var,
            g_matrix,
            relations,
        )
        self.assertEqual(
            len(prob.constraints),
            len(relations) + 1,
        )

    def test_generate_uta_problem(self):
        u_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        sigma_var = generate_alternatives_errors_variables(self.alternatives)
        prob = generate_uta_problem(
            self.normalized_table, u_var, sigma_var, g_matrix, self.relations
        )
        self.assertIsNotNone(prob.objective)
        self.assertEqual(prob.sense, LpMinimize)

    def test_generate_uta_star_problem(self):
        w_var = generate_marginal_utility_variables(self.criteria_segments)
        g_matrix = generate_criteria_values_matrix(self.criteria_segments)
        sigma_p_var = generate_alternatives_errors_variables(
            self.alternatives, "sigma_p"
        )
        sigma_n_var = generate_alternatives_errors_variables(
            self.alternatives, "sigma_n"
        )
        prob = generate_uta_star_problem(
            self.normalized_table,
            w_var,
            sigma_p_var,
            sigma_n_var,
            g_matrix,
            self.relations,
        )
        self.assertIsNotNone(prob.objective)
        self.assertEqual(prob.sense, LpMinimize)

    def test_normalized_uta(self):
        functions = normalized_uta(
            self.normalized_table,
            self.criteria_segments,
            self.relations,
        )
        self.assertEqual(len(functions), len(self.criteria_segments))
        res = apply_criteria_functions(self.normalized_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())

    def test_normalized_uta_star(self):
        functions = normalized_uta_star(
            self.normalized_table,
            self.criteria_segments,
            self.relations,
        )
        self.assertEqual(len(functions), len(self.criteria_segments))
        res = apply_criteria_functions(self.normalized_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())

    def test_post_optimality(self):
        functions = post_optimality(
            self.normalized_table, self.criteria_segments, self.relations, 0.01
        )
        self.assertEqual(len(functions), len(self.criteria_segments))
        res = apply_criteria_functions(self.normalized_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())

    def test_star_post_optimality(self):
        functions = star_post_optimality(
            self.normalized_table, self.criteria_segments, self.relations, 0.01
        )
        self.assertEqual(len(functions), len(self.criteria_segments))
        res = apply_criteria_functions(self.normalized_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())

    def test_uta(self):
        functions = uta(
            self.performance_table,
            self.scales,
            self.criteria_segments,
            self.relations,
            delta=0.01,
        )
        res = apply_criteria_functions(self.performance_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())
        functions = uta(
            self.performance_table,
            self.scales,
            self.criteria_segments,
            self.relations,
            delta=0.01,
            post_optimality_=True,
        )
        res = apply_criteria_functions(self.performance_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())

    def test_uta_star(self):
        functions = uta_star(
            self.performance_table,
            self.scales,
            self.criteria_segments,
            self.relations,
            delta=0.01,
        )
        res = apply_criteria_functions(self.performance_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())
        functions = uta_star(
            self.performance_table,
            self.scales,
            self.criteria_segments,
            self.relations,
            delta=0.01,
            post_optimality_=True,
        )
        res = apply_criteria_functions(self.performance_table, functions)
        grades = sum_table(res, axis=1)
        ranks = grades.sort_values(ascending=False)
        self.assertEqual(grades.index.tolist(), ranks.index.tolist())
