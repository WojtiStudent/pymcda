import pytest
from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.core.scales import QuantitativeScale
from mcda.outranking.electre3 import (
    concordance,
    credibility,
    discordance,
    distillation,
    electre_iii,
    qualification,
)


@pytest.fixture
def dataset():
    return DataFrame(
        [
            [8.84, 8.79, 6.43, 6.95],
            [8.57, 8.51, 5.47, 6.91],
            [7.76, 7.75, 5.34, 8.76],
            [7.97, 9.12, 5.93, 8.09],
            [9.03, 8.97, 8.19, 8.10],
            [7.41, 7.87, 6.77, 7.23],
        ]
    )


@pytest.fixture
def weight():
    return {0: 9.00, 1: 8.24, 2: 5.98, 3: 8.48}


@pytest.fixture
def scales():
    return {
        0: QuantitativeScale(7, 10),
        1: QuantitativeScale(7, 10),
        2: QuantitativeScale(5, 9),
        3: QuantitativeScale(6, 9),
    }


@pytest.fixture
def preference():
    return {0: 0.50, 1: 0.50, 2: 0.50, 3: 0.50}


@pytest.fixture
def indifference():
    return {0: 0.30, 1: 0.30, 2: 0.30, 3: 0.30}


@pytest.fixture
def veto():
    return {0: 0.70, 1: 0.70, 2: 0.70, 3: 0.70}


def test_concordance_matrix(dataset, weight, scales, preference, indifference):
    concordance_matrix = DataFrame(
        [
            [1.0, 1.0, 0.73, 0.69, 0.54, 0.96],
            [0.81, 1.0, 0.73, 0.32, 0.11, 0.78],
            [0.27, 0.46, 1.0, 0.55, 0.27, 0.81],
            [0.53, 0.72, 0.73, 1.0, 0.53, 0.81],
            [1.0, 1.0, 0.73, 1.0, 1.0, 1.0],
            [0.46, 0.46, 0.66, 0.19, 0.0, 1.0],
        ]
    )
    # Absolute value = 0.006 according to usual math rounding method
    # (if last digit >5 => round to superior else otherwise
    assert_frame_equal(
        concordance(dataset, weight, scales, preference, indifference),
        concordance_matrix,
        atol=0.006,
    )

    indifference = {0: 1.30, 1: 1.30, 2: 1.30, 3: 1.30}

    with pytest.raises(ValueError):
        concordance(dataset, weight, scales, preference, indifference)


def test_discordance_matrix(dataset, scales, preference, veto):
    discordance_mat = [
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 1],
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 0, 0],
        ],
        [
            [0, 0, 1, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 1],
            [0, 0.5499999999999973, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
        ],
        [
            [1, 1, 1, 0],
            [1, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 1, 0.4499999999999994, 0],
            [1, 1, 1, 0],
            [0, 0, 1, 0],
        ],
        [
            [1, 0, 0, 0],
            [0.5000000000000028, 0, 0, 0],
            [0, 0, 0, 0.8499999999999999],
            [0, 0, 0, 0],
            [1, 0, 1, 0],
            [0, 0, 1, 0],
        ],
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0.8000000000000009],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [1, 1, 0, 0],
            [1, 0.6999999999999985, 0, 0],
            [0, 0, 0, 1],
            [0.2999999999999981, 1, 0, 1],
            [1, 1, 1, 1],
            [0, 0, 0, 0],
        ],
    ]
    discordance_mat = DataFrame(
        [
            [Series({i: x for i, x in enumerate(aaa)}) for aaa in aa]
            for aa in discordance_mat
        ]
    )
    calculated_discordance_mat = discordance(dataset, scales, preference, veto)

    # Absolute value = 0.006 according to usual math rounding method
    # (if last digit >5 => round to superior else otherwise
    assert_frame_equal(calculated_discordance_mat, discordance_mat, atol=0.006)

    veto = {0: 0.20, 1: 0.20, 2: 0.20, 3: 0.20}

    with pytest.raises(ValueError):
        discordance(dataset, scales, preference, veto)


def test_credibility_matrix(
    dataset, weight, scales, preference, indifference, veto
):
    credibility_mat = DataFrame(
        [
            [1, 1, 0, 0, 0, 0.96227129],
            [0, 1, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 1, 0.0, 0.0, 0.0],
            [0.0, 0.71608833, 0.41073113, 1, 0.0, 0.0],
            [1.0, 1.0, 0.54764151, 1.0, 1, 1.0],
            [0.0, 0, 0.0, 0.0, 0.0, 1],
        ]
    )

    calculated_credibility_mat = credibility(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance,
        discordance,
    )

    assert_frame_equal(calculated_credibility_mat, credibility_mat, atol=0.006)

    veto = {0: None, 1: None, 2: None, 3: None, 4: None}

    credibility_mat = DataFrame(
        [
            [1.0, 1.0, 0.73, 0.69, 0.54, 0.96],
            [0.81, 1.0, 0.73, 0.32, 0.11, 0.78],
            [0.27, 0.46, 1.0, 0.55, 0.27, 0.81],
            [0.53, 0.72, 0.73, 1.0, 0.53, 0.81],
            [1.0, 1.0, 0.73, 1.0, 1.0, 1.0],
            [0.46, 0.46, 0.66, 0.19, 0.0, 1.0],
        ]
    )
    calculated_credibility_mat = credibility(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance,
        discordance,
    )
    assert_frame_equal(calculated_credibility_mat, credibility_mat, atol=0.006)


def test_qualification(
    dataset,
    weight,
    scales,
    preference,
    indifference,
    veto,
    alpha=0.30,
    beta=-0.15,
):
    qualification_list = Series(
        {0: 1.0, 1: -2.0, 2: 0.0, 3: -1.0, 4: 4.0, 5: -2.0}
    )

    calculated_qualification = qualification(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance,
        discordance,
        credibility,
        alpha,
        beta,
    )
    assert_series_equal(
        calculated_qualification,
        qualification_list,
        atol=0.006,
        check_dtype=False,
    )


def test_distillation(
    dataset,
    weight,
    scales,
    preference,
    indifference,
    veto,
    alpha=0.30,
    beta=-0.15,
):
    descending_distillation = [[4], [0], [1, 2, 3, 5]]
    ascending_distillation = [[2, 4], [0, 3], [1, 5]]

    calculated_dist = distillation(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance,
        discordance,
        credibility,
        alpha,
        beta,
    )
    assert calculated_dist == descending_distillation

    calculated_dist = distillation(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance,
        discordance,
        credibility,
        alpha,
        beta,
        ascending=True,
    )
    assert calculated_dist == ascending_distillation


def test_electre_iii(dataset, weight, scales, preference, indifference, veto):
    # Test with default parameters
    calculated_ranking = electre_iii(
        dataset, weight, scales, preference, indifference, veto
    )
    ranking = DataFrame(
        [
            [1, 1, 0, 1, 0, 1],
            [0, 1, 0, 0, 0, 1],
            [0, 1, 1, 1, 0, 1],
            [0, 1, 0, 1, 0, 1],
            [1, 1, 1, 1, 1, 1],
            [0, 1, 0, 0, 0, 1],
        ]
    )

    assert_frame_equal(ranking, calculated_ranking, check_dtype=False)

    # Test specific parameters
    calculated_ranking = electre_iii(
        dataset,
        weight,
        scales,
        preference,
        indifference,
        veto,
        concordance_function=concordance,
        discordance_function=discordance,
        credibility_function=credibility,
        alpha=0.30,
        beta=-0.15,
    )
    assert_frame_equal(ranking, calculated_ranking, check_dtype=False)
