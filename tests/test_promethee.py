import unittest

from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.core.relations import RelationType
from mcda.core.scales import PreferenceDirection, QuantitativeScale
from mcda.outranking.promethee1 import promethee1
from mcda.outranking.promethee2 import promethee2
from mcda.outranking.promethee_common import (
    PreferenceFunction,
    negative_preference_flow_calculation,
    positive_preference_flow_calculation,
    preference_degree_calculation,
    scale_performance_table,
)

dataset = DataFrame(
    [
        [1, 2, -1, 5, 2, 2],  # a1
        [3, 5, 3, -5, 3, 3],  # a2
        [3, -5, 3, 4, 3, 2],  # a3
        [2, -2, 2, 5, 1, 1],  # a4
        [3, 5, 3, -5, 3, 3],  # a5
    ]
)

functions = {
    0: PreferenceFunction.USUAL,
    1: PreferenceFunction.U_SHAPE,
    2: PreferenceFunction.V_SHAPE,
    3: PreferenceFunction.LEVEL,
    4: PreferenceFunction.LINEAR,
    5: PreferenceFunction.GAUSSIAN,
}

scales = {
    0: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    1: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    2: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    3: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    4: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    5: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
}

scales_min = {
    0: QuantitativeScale(-5, 5, PreferenceDirection.MIN),
    1: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    2: QuantitativeScale(-5, 5, PreferenceDirection.MIN),
    3: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    4: QuantitativeScale(-5, 5, PreferenceDirection.MAX),
    5: QuantitativeScale(-5, 5, PreferenceDirection.MIN),
}

p_param = {0: 1, 1: 4, 2: 3, 3: 1, 4: 2, 5: 2}

q_param = {0: 0.2, 1: 2.5, 2: 2, 3: 0.5, 4: 1, 5: 0.5}

s_param = {0: 2, 1: 0.5, 2: 1, 3: 1, 4: 3, 5: 1}

weights = {0: 0.5, 1: 3, 2: 1.5, 3: 0.2, 4: 2, 5: 1}


class TestPrometheeMethods(unittest.TestCase):
    def test_pref_calc_errors(self):
        with self.assertRaises(ValueError) as context:
            preference_degree_calculation(
                PreferenceFunction.LEVEL, 1, 2, 0, 1, 0
            )
        self.assertTrue(
            "incorrect threshold : q 2 greater than p 1"
            in str(context.exception)
        )

        with self.assertRaises(ValueError) as context:
            preference_degree_calculation(
                PreferenceFunction.LINEAR, 1, 2, 0, 1, 0
            )
        self.assertTrue(
            "incorrect threshold : q 2 greater than p 1"
            in str(context.exception)
        )

        with self.assertRaises(ValueError) as context:
            preference_degree_calculation("USUAL", 1, 0, 0, 1, 0)
        self.assertTrue(
            "pref_func USUAL is not known. \n See PreferenceFunction Enum"
            in str(context.exception)
        )

    def test_flow_plus(self):
        flow_plus_list = []
        for i in range(len(dataset)):
            flow_plus_list.append(
                positive_preference_flow_calculation(
                    dataset.loc[i],
                    dataset.drop(i),
                    functions,
                    p_param,
                    q_param,
                    s_param,
                    weights,
                )
            )

        result = [0.84607, 1.90874, 0.70652, 0.67073, 1.90874]
        for calc_flow, test_flow in zip(flow_plus_list, result):
            self.assertAlmostEqual(
                calc_flow,
                test_flow,
                None,
                "Assertion error, in positive flow",
                0.01,
            )

    def test_flow_minus(self):
        flow_minus_list = []
        for i in range(len(dataset)):
            flow_minus_list.append(
                negative_preference_flow_calculation(
                    dataset.loc[i],
                    dataset.drop(i),
                    functions,
                    p_param,
                    q_param,
                    s_param,
                    weights,
                )
            )

        result = [1.80328, 0.07317, 1.58378, 2.50200, 0.07317]
        for calc_flow, test_flow in zip(flow_minus_list, result):
            self.assertAlmostEqual(
                calc_flow,
                test_flow,
                None,
                "Assertion error, in negative flow",
                0.01,
            )

    def test_scaling_performance_table(self):
        perf_table = scale_performance_table(dataset, scales_min)

        scaled_dataset = DataFrame(
            [
                [-1, 2, 1, 5, 2, -2],  # a1
                [-3, 5, -3, -5, 3, -3],  # a2
                [-3, -5, -3, 4, 3, -2],  # a3
                [-2, -2, -2, 5, 1, -1],  # a4
                [-3, 5, -3, -5, 3, -3],  # a5
            ]
        )

        assert_frame_equal(perf_table, scaled_dataset, check_dtype=False)

    # Test for promethee1
    def test_partial_order(self):
        partial_order_list = promethee1(
            dataset,
            functions,
            scales,
            p_param,
            q_param,
            s_param,
            weights,
        )
        print(partial_order_list)

        relations = [
            (1, 0, RelationType.PREFERENCE),
            (0, 2, RelationType.INCOMPARABLE),
            (0, 3, RelationType.PREFERENCE),
            (4, 0, RelationType.PREFERENCE),
            (1, 2, RelationType.PREFERENCE),
            (1, 3, RelationType.PREFERENCE),
            (1, 4, RelationType.INDIFFERENCE),
            (2, 3, RelationType.PREFERENCE),
            (4, 2, RelationType.PREFERENCE),
            (4, 3, RelationType.PREFERENCE),
        ]
        # pref_matrix = [
        #     [0, 0, 0, 1, 0],
        #     [1, 1, 0, 1, 0],
        #     [0, 0, 0, 1, 0],
        #     [0, 0, 0, 0, 0],
        #     [1, 0, 1, 1, 0],
        # ]
        # ind_matrix = [
        #     [0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 1],
        #     [0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0],
        #     [0, 1, 0, 0, 0],
        # ]
        # incomp_matrix = [
        #     [0, 0, 1, 0, 0],
        #     [0, 0, 0, 0, 0],
        #     [1, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0],
        # ]

        assert partial_order_list == relations

    # Test for promethee2
    def test_total_order(self):
        tot_order = promethee2(
            dataset,
            functions,
            scales,
            p_param,
            q_param,
            s_param,
            weights,
        )
        check_val = Series([-0.96261, 1.83557, -0.87726, -1.83127, 1.83557])

        assert_series_equal(check_val, tot_order)
