from unittest import TestCase

from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.core.functions import DiscreteFunction
from mcda.core.performance_table import (
    apply_criteria_functions,
    apply_criteria_weights,
    get_alternative_values,
    get_alternative_values_at,
    get_criterion_values,
    get_criterion_values_at,
    is_numeric,
    is_within_criteria_scales,
    normalize,
    normalize_without_scales,
    sum_table,
    transform,
    within_criteria_scales,
)
from mcda.core.scales import (
    PreferenceDirection,
    QualitativeScale,
    QuantitativeScale,
)


class TestPerformanceTable(TestCase):
    def setUp(self) -> None:
        self.alternatives = ["a01", "a02", "a03", "a04"]
        self.criteria = ["c01", "c02", "c03"]
        self.scales = {
            "c01": QualitativeScale(["*", "**", "***", "****"], [1, 2, 3, 4]),
            "c02": QualitativeScale(
                ["Perfect", "Good", "Bad"], [1, 2, 3], PreferenceDirection.MIN
            ),
            "c03": QuantitativeScale(0, 25000, PreferenceDirection.MIN),
        }
        self.performance_table = DataFrame(
            [
                ["*", "Good", 5000],
                ["***", "Bad", 12000],
                ["**", "Perfect", 8500],
                ["****", "Good", 18635.2],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        self.numeric_table = DataFrame(
            [
                [1, 2, 5000],
                [3, 3, 12000],
                [2, 1, 8500],
                [4, 2, 18635.2],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )

    def test_is_numeric(self):
        self.assertFalse(is_numeric(self.performance_table))
        self.assertTrue(is_numeric(self.numeric_table))

    def test_apply_criteria_functions(self):
        functions = {
            "c01": DiscreteFunction({"*": 0, "**": 1, "***": 2, "****": 3}),
            "c02": lambda x: f"Very {x}",
            "c03": lambda x: x * 2,
        }
        res = DataFrame(
            [
                [0, "Very Good", 10000],
                [2, "Very Bad", 24000],
                [1, "Very Perfect", 17000],
                [3, "Very Good", 37270.4],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        assert_frame_equal(
            apply_criteria_functions(self.performance_table, functions), res
        )

    def test_apply_criteria_weights(self):
        weights = {"c01": 1, "c02": 0.5, "c03": 2.0}
        res = apply_criteria_weights(self.numeric_table, weights)
        for criterion, weight in weights.items():
            assert_series_equal(
                res[criterion], weight * self.numeric_table[criterion]
            )

    def test_transform(self):
        out_scales = {
            "c01": self.scales["c01"].quantitative,
            "c02": self.scales["c02"].quantitative,
            "c03": self.scales["c03"],
        }
        res = transform(self.performance_table, self.scales, out_scales)
        assert_frame_equal(self.numeric_table, res, check_dtype=False)

    def test_normalize_without_scales(self):
        res = DataFrame(
            [
                [0, 0.5, 0],
                [2 / 3, 1, 7000 / 13635.2],
                [1 / 3, 0, 3500 / 13635.2],
                [1, 0.5, 1],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        assert_frame_equal(normalize_without_scales(self.numeric_table), res)
        self.assertRaises(
            TypeError, normalize_without_scales, self.performance_table
        )

    def test_normalize(self):
        res = DataFrame(
            [
                [0, 0.5, (25000 - 5000) / 25000],
                [2 / 3, 0, (25000 - 12000) / 25000],
                [1 / 3, 1, (25000 - 8500) / 25000],
                [1, 0.5, (25000 - 18635.2) / 25000],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        assert_frame_equal(normalize(self.performance_table, self.scales), res)
        assert_frame_equal(
            normalize(self.numeric_table),
            normalize_without_scales(self.numeric_table),
        )

    def test_get_alternative_values(self):
        res = Series(["***", "Bad", 12000], index=self.criteria, name="a02")
        assert_series_equal(
            get_alternative_values(self.performance_table, "a02"), res
        )

    def test_get_alternative_values_at(self):
        res = Series(["***", "Bad", 12000], index=self.criteria, name="a02")
        assert_series_equal(
            get_alternative_values_at(self.performance_table, 1), res
        )

    def test_get_criterion_values(self):
        res = Series(
            ["Good", "Bad", "Perfect", "Good"],
            index=self.alternatives,
            name="c02",
        )
        assert_series_equal(
            get_criterion_values(self.performance_table, "c02"), res
        )

    def test_get_criterion_values_at(self):
        res = Series(
            ["Good", "Bad", "Perfect", "Good"],
            index=self.alternatives,
            name="c02",
        )
        assert_series_equal(
            get_criterion_values_at(self.performance_table, 1), res
        )

    def test_is_within_criteria_scales(self):
        self.assertTrue(
            is_within_criteria_scales(self.performance_table, self.scales)
        )
        table = DataFrame(
            [
                ["*", "Good", 5000],
                ["Stars", "Bad", 12000],
                ["**", "Perfect", 8500],
                ["****", "Good", 18635.2],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        self.assertFalse(is_within_criteria_scales(table, self.scales))

    def test_within_criteria_scales(self):
        res = within_criteria_scales(self.performance_table, self.scales)
        self.assertTrue(res.all(None))
        table = DataFrame(
            [
                ["*", "Good", 5000],
                ["Stars", "Bad", 12000],
                ["**", "Perfect", 8500],
                ["****", "Good", 18635.2],
            ],
            index=self.alternatives,
            columns=self.criteria,
        )
        self.assertFalse(
            within_criteria_scales(table, self.scales).loc["a02", "c01"]
        )

    def test_sum_table(self):
        self.assertEqual(
            sum_table(self.performance_table),
            self.performance_table["c03"].sum(),
        )
        assert_series_equal(
            sum_table(self.performance_table, 0),
            Series({"c03": self.performance_table["c03"].sum()}),
        )
        table = DataFrame([[0, 1, 2, 3], [1, 2, 3, 4], [2, 3, 4, 5]])
        self.assertEqual(sum_table(table), 30)
