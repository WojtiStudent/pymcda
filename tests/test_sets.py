import unittest

from mcda.core.sets import (
    cardinal_1_subsets,
    cardinal_range,
    cardinal_subsets,
    cardinality,
    complement,
    complement_subsets,
    complete_set,
    disjoint_union,
    index_to_set,
    int_to_mask,
    intersection,
    mask_to_int,
    one_bit_extracted_set,
    proper_subsets,
    set_to_index,
    size,
    subsets,
    union,
)


def test_mask_to_int():
    assert mask_to_int("110") == 6
    assert mask_to_int("00001") == 1


def test_int_to_mask():
    assert int_to_mask(6) == "110"
    assert int_to_mask(19) == "10011"


class SetTestCase(unittest.TestCase):
    def setUp(self):
        self.set_ = {"c", "e", "a"}
        self.set_2 = {"a", "d"}
        self.set_index = 21
        self.set_index2 = 9
        self.ensemble = ["a", "b", "c", "d", "e"]
        self.ensemble_size = 2 ** 5

    def test_intersection(self):
        self.assertEqual(intersection(self.set_index, self.set_index2), 1)

    def test_union(self):
        self.assertEqual(union(self.set_index, self.set_index2), 29)

    def test_disjoint_union(self):
        self.assertEqual(disjoint_union(self.set_index, self.set_index2), 28)

    def test_complement(self):
        self.assertEqual(complement(self.set_index), 10)
        self.assertEqual(complement(self.set_index, 5), 10)

    def test_cardinality(self):
        self.assertEqual(cardinality(self.set_index), 3)

    def test_size(self):
        self.assertEqual(size(self.set_index), 5)

    def test_complete_set(self):
        self.assertEqual(complete_set(5), 2 ** 5 - 1)

    def test_one_bit_extracted_set(self):
        self.assertEqual(one_bit_extracted_set(self.set_index), {5, 17, 20})

    def test_cardinal_1_subsets(self):
        self.assertEqual(cardinal_1_subsets(self.set_index), {1, 4, 16})

    def test_cardinal_subsets(self):
        self.assertEqual(
            cardinal_subsets(self.set_index, 1),
            cardinal_1_subsets(self.set_index),
        )
        self.assertEqual(
            cardinal_subsets(self.set_index, 2),
            one_bit_extracted_set(self.set_index),
        )

    def test_proper_subsets(self):
        self.assertEqual(
            proper_subsets(self.set_index), {0, 1, 4, 5, 16, 17, 20}
        )

    def test_subsets(self):
        self.assertEqual(subsets(self.set_index), {0, 1, 4, 5, 16, 17, 20, 21})

    def test_complement_subsets(self):
        self.assertEqual(complement_subsets(self.set_index, 5), {0, 2, 8, 10})

    def test_cardinal_range(self):
        res = cardinal_range(2 ** 3)
        expected = [0, 1, 2, 4, 3, 5, 6, 7]
        for a, b in zip(res, expected):
            self.assertEqual(a, b)

    def test_set_to_index(self):
        self.assertEqual(
            set_to_index(self.set_, self.ensemble), self.set_index
        )

    def test_index_to_set(self):
        self.assertEqual(
            index_to_set(self.set_index, self.ensemble), self.set_
        )
