import pytest
from pandas import DataFrame, Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.core.performance_table import normalize
from mcda.core.relations import RelationType
from mcda.core.scales import (
    PreferenceDirection,
    QualitativeScale,
    QuantitativeScale,
)
from mcda.outranking.srmp import (
    PreferenceElicitationResult,
    denormalize,
    denormalize_value,
    normalized_preference_elicitation,
    preference_elicitation,
    preference_relation,
    ranking,
    srmp,
)


@pytest.fixture
def dataset():
    return DataFrame(
        [
            [0.720, 3.560, 1.340, 0.62, 44.340],
            [0.8, 3.940, 1.430, 0.74, 36.360],
            [0.760, 3.630, 1.380, 0.89, 48.750],
            [0.780, 3.740, 1.450, 0.72, 42.130],
            [0.740, 3.540, 1.370, 0.73, 36.990],
            [0.690, 3.740, 1.450, 0.84, 42.430],
            [0.7, 3.280, 1.280, 0.83, 47.430],
            [0.860, 3.370, 1.150, 0.8, 80.790],
        ]
    )


@pytest.fixture
def norm_dataset(dataset, scales):
    return normalize(dataset, scales)


@pytest.fixture
def weight():
    return {0: 30, 1: 30, 2: 20, 3: 10, 4: 10}


@pytest.fixture
def scales():
    return {
        0: QuantitativeScale(0, 1),
        1: QuantitativeScale(3, 4),
        2: QuantitativeScale(1, 2),
        3: QuantitativeScale(0, 1),
        4: QuantitativeScale(30, 100, PreferenceDirection.MIN),
    }


@pytest.fixture
def profiles():
    return DataFrame(
        [
            [0.750, 3.500, 1.300, 0.730, 43.00],
            [0.800, 3.700, 1.370, 0.790, 42.000],
        ]
    )


@pytest.fixture
def lexicographic_order():
    return [1, 0]


@pytest.fixture
def relations():
    return [
        (1, 0, RelationType.PREFERENCE),
        (3, 2, RelationType.PREFERENCE),
        (3, 4, RelationType.PREFERENCE),
        (5, 4, RelationType.PREFERENCE),
        (5, 6, RelationType.PREFERENCE),
        (6, 7, RelationType.INDIFFERENCE),
        (7, 0, RelationType.INDIFFERENCE),
        (6, 0, RelationType.INDIFFERENCE),
        (1, 3, RelationType.PREFERENCE),
        (2, 6, RelationType.PREFERENCE),
    ]


def test_preference_relation(dataset, weight, scales, profiles):
    preference_matrix_0 = DataFrame(
        [
            [1, 0, 0, 0, 0, 0, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 1, 1, 1, 1],
            [1, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 1, 1],
        ]
    )

    preference_matrix_1 = DataFrame(
        [
            [1, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [1, 0, 1, 1, 1, 0, 1, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1, 0],
            [1, 0, 1, 0, 1, 0, 1, 1],
        ]
    )

    calculated_preference_matrix_0 = preference_relation(
        profiles.iloc[0], dataset, scales, weight
    )

    calculated_preference_matrix_1 = preference_relation(
        profiles.iloc[1], dataset, scales, weight
    )

    assert_frame_equal(preference_matrix_0, calculated_preference_matrix_0)
    assert_frame_equal(preference_matrix_1, calculated_preference_matrix_1)


def test_ranking(lexicographic_order):
    preference_matrix_0 = DataFrame(
        [
            [1, 0, 0, 0, 0, 0, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 1, 1, 1, 1],
            [1, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 1, 1],
        ]
    )

    preference_matrix_1 = DataFrame(
        [
            [1, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [1, 0, 1, 1, 1, 0, 1, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1, 0],
            [1, 0, 1, 0, 1, 0, 1, 1],
        ]
    )

    rank = Series([1, 8, 4, 6, 3, 7, 2, 5])

    calculated_rank = ranking(
        [preference_matrix_0, preference_matrix_1], lexicographic_order
    )

    assert_series_equal(rank, calculated_rank)


def test_srmp(dataset, weight, scales, profiles, lexicographic_order):
    rank = Series([1, 8, 4, 6, 3, 7, 2, 5])

    calculated_rank = srmp(
        dataset, scales, weight, profiles, lexicographic_order
    )

    assert_series_equal(rank, calculated_rank)


def test_normalized_preference_elicitation(
    norm_dataset,
    relations,
    lexicographic_order,
):
    preference_elicitation_result: PreferenceElicitationResult = {
        "criteria_weights": {
            0: 0.2,
            1: 0.5,
            2: 0.1,
            3: 0.1,
            4: 0.1,
        },
        "profiles": DataFrame(
            [
                [0.0, 0.73, 0.0, 0.0, 0.0091428571],
                [1.0, 0.94, 1.0, 0.62, 0.27442857],
            ]
        ),
        "lexicographic_order": [1, 0],
        "fitness": 0.9,
    }

    calculated_preference_elicitation_result = (
        normalized_preference_elicitation(
            norm_dataset,
            relations,
            lexicographic_order,
            inconsistencies=True,
            gamma=0.1,
            non_dictator=True,
            solver_args=None,
        )
    )

    assert (
        preference_elicitation_result.keys()
        == calculated_preference_elicitation_result.keys()
    )
    assert (
        preference_elicitation_result["criteria_weights"]
        == calculated_preference_elicitation_result["criteria_weights"]
    )
    assert_frame_equal(
        preference_elicitation_result["profiles"],
        calculated_preference_elicitation_result["profiles"],
    )
    assert (
        preference_elicitation_result["lexicographic_order"]
        == calculated_preference_elicitation_result["lexicographic_order"]
    )
    assert (
        preference_elicitation_result["fitness"]
        == calculated_preference_elicitation_result["fitness"]
    )


def test_denormalize_value():
    x = 0.51
    scale = QuantitativeScale(1, 3)
    denormalized_value = 2.02
    calculated_denormalized_value = denormalize_value(x, scale)
    assert denormalized_value == calculated_denormalized_value

    x = 0.51
    scale = QuantitativeScale(1, 3, PreferenceDirection.MIN)
    denormalized_value = 1.98
    calculated_denormalized_value = denormalize_value(x, scale)
    assert denormalized_value == calculated_denormalized_value

    x = 0.51
    scale = QualitativeScale(["Evil", "Neutral", "Good"], [1, 2, 3])
    denormalized_value = "Good"
    calculated_denormalized_value = denormalize_value(x, scale)
    assert denormalized_value == calculated_denormalized_value


def test_denormalize(norm_dataset, scales, dataset):
    assert_frame_equal(dataset, denormalize(norm_dataset, scales))

    scale = QualitativeScale(["Evil", "Neutral", "Good"], [0, 0.5, 1])
    modified_scales = scales.copy()
    modified_scales[0] = scale
    modified_dataset = dataset.copy(deep=True)
    modified_dataset[0] = [
        "Evil",
        "Neutral",
        "Good",
        "Evil",
        "Neutral",
        "Good",
        "Evil",
        "Neutral",
    ]
    modified_norm_dataset = normalize(modified_dataset, modified_scales)

    assert_frame_equal(
        modified_dataset, denormalize(modified_norm_dataset, modified_scales)
    )

    modified_norm_dataset.iat[0, 0] = 0.1
    modified_dataset.iat[0, 0] = "Neutral"

    assert_frame_equal(
        modified_dataset, denormalize(modified_norm_dataset, modified_scales)
    )


def test_preference_elicitation(dataset, scales, relations):
    preference_elicitation_result: PreferenceElicitationResult = {
        "criteria_weights": {
            0: 0.5,
            1: 0.1,
            2: 0.2,
            3: 0.1,
            4: 0.1,
        },
        "profiles": DataFrame(
            [
                [0.96, 3.47, 1.00, 0.72, 99.36],
                [0.96, 3.73, 1.55, 0.99, 99.36],
                [1.00, 3.94, 2.00, 1.00, 80.79],
            ]
        ),
        "lexicographic_order": [0, 1, 2],
        "fitness": 1,
    }

    calculated_preference_elicitation_result = preference_elicitation(
        dataset,
        scales,
        relations,
        max_profiles_number=3,
        inconsistencies=True,
        gamma=0.1,
        non_dictator=True,
        solver_args=None,
    )

    assert (
        preference_elicitation_result.keys()
        == calculated_preference_elicitation_result.keys()
    )
    assert (
        preference_elicitation_result["criteria_weights"]
        == calculated_preference_elicitation_result["criteria_weights"]
    )
    assert_frame_equal(
        preference_elicitation_result["profiles"],
        calculated_preference_elicitation_result["profiles"],
    )
    assert (
        preference_elicitation_result["lexicographic_order"]
        == calculated_preference_elicitation_result["lexicographic_order"]
    )
    assert (
        preference_elicitation_result["fitness"]
        == calculated_preference_elicitation_result["fitness"]
    )
