import pytest
from pandas import DataFrame
from pandas.testing import assert_frame_equal

from mcda.core.scales import QuantitativeScale
from mcda.outranking.electre1 import concordance, discordance
from mcda.outranking.electre2 import (
    distillation,
    electre_ii,
    final_ranking,
    outranking,
)


@pytest.fixture
def dataset():
    return DataFrame(
        [
            [1, 2, 1, 5, 2, 2, 4],
            [3, 5, 3, 5, 3, 3, 3],
            [3, 5, 3, 5, 3, 2, 2],
            [1, 2, 2, 5, 1, 1, 1],
            [1, 1, 3, 5, 4, 1, 5],
        ]
    )


@pytest.fixture
def weight():
    return {
        0: 0.0780,
        1: 0.1180,
        2: 0.1570,
        3: 0.3140,
        4: 0.2350,
        5: 0.0390,
        6: 0.0590,
    }


@pytest.fixture
def scales():
    return {
        0: QuantitativeScale(1, 5),
        1: QuantitativeScale(1, 5),
        2: QuantitativeScale(1, 5),
        3: QuantitativeScale(1, 5),
        4: QuantitativeScale(1, 5),
        5: QuantitativeScale(1, 5),
        6: QuantitativeScale(1, 5),
    }


def test_outranking(dataset, weight, scales):
    s_outranking_matrix = DataFrame(
        [
            [1.0, 0.0, 0.0, 0.0, 0.0],
            [1.0, 1.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 1.0],
        ]
    )
    w_outranking_matrix = DataFrame(
        [
            [1.0, 0.0, 0.0, 1.0, 0.0],
            [1.0, 1.0, 1.0, 1.0, 0.0],
            [1.0, 0.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 0.0],
            [1.0, 0.0, 0.0, 1.0, 1.0],
        ]
    )
    c_hat_sup = 0.85
    c_hat_inf = 0.65
    d_hat_sup = 0.50
    d_hat_inf = 0.25

    concordance_mat = concordance(dataset, weight, scales)
    discordance_mat = discordance(dataset, scales)

    s_outranking_calculated = outranking(
        concordance_mat, discordance_mat, c_hat_sup, d_hat_inf
    )
    w_outranking_calculated = outranking(
        concordance_mat, discordance_mat, c_hat_inf, d_hat_sup
    )

    assert_frame_equal(
        w_outranking_matrix, w_outranking_calculated, check_dtype=False
    )
    assert_frame_equal(
        s_outranking_matrix, s_outranking_calculated, check_dtype=False
    )


def test_distillation(dataset, weight, scales):
    desc_distillate = [[1, 4], [2], [0], [3]]
    asc_distillate = [[1], [2, 4], [0], [3]]

    s_outranking_matrix = DataFrame(
        [
            [1.0, 0.0, 0.0, 0.0, 0.0],
            [1.0, 1.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 1.0],
        ]
    )
    w_outranking_matrix = DataFrame(
        [
            [1.0, 0.0, 0.0, 1.0, 0.0],
            [1.0, 1.0, 1.0, 1.0, 0.0],
            [1.0, 0.0, 1.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0, 0.0],
            [1.0, 0.0, 0.0, 1.0, 1.0],
        ]
    )

    calculated_distillate = distillation(
        s_outranking_matrix, w_outranking_matrix
    )
    assert desc_distillate == calculated_distillate
    calculated_distillate = distillation(
        s_outranking_matrix, w_outranking_matrix, ascending=True
    )
    assert asc_distillate == calculated_distillate


def test_final_ranking(dataset, weight):
    final_rank = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1],
            [1, 0, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [1, 0, 1, 1, 1],
        ]
    )
    descending_dist_matrix = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1],
            [1, 0, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [1, 1, 1, 1, 1],
        ]
    )
    ascending_dist_matrix = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1],
            [0, 0, 0, 1, 0],
            [1, 0, 1, 1, 1],
        ]
    )
    calculated_final_rank = final_ranking(
        ascending_dist_matrix, descending_dist_matrix
    )
    assert_frame_equal(calculated_final_rank, final_rank, check_dtype=False)


def test_electre_ii(dataset, weight, scales):
    final_rank = DataFrame(
        [
            [1, 0, 0, 1, 0],
            [1, 1, 1, 1, 1],
            [1, 0, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [1, 0, 1, 1, 1],
        ]
    )

    c_hat_sup = 0.85
    c_hat_inf = 0.65
    d_hat_sup = 0.50
    d_hat_inf = 0.25
    calculated_final_rank = electre_ii(
        dataset,
        weight,
        scales,
        c_hat_sup,
        c_hat_inf,
        d_hat_sup,
        d_hat_inf,
    )
    assert_frame_equal(calculated_final_rank, final_rank, check_dtype=False)

    concordance_matrix = DataFrame(
        [
            [1.0, 0.37, 0.41, 0.84, 0.55],
            [0.94, 1.0, 1.0, 1.0, 0.71],
            [0.94, 0.9, 1.0, 1.0, 0.71],
            [0.67, 0.31, 0.31, 1.0, 0.55],
            [0.84, 0.77, 0.77, 0.88, 1.0],
        ]
    )
    discordance_matrix = DataFrame(
        [
            [0.0, 0.75, 0.75, 0.25, 0.5],
            [0.25, 0.0, 0.0, 0.0, 0.5],
            [0.5, 0.25, 0.0, 0.0, 0.75],
            [0.75, 0.75, 0.75, 0.0, 1.0],
            [0.25, 1.0, 1.0, 0.25, 0.0],
        ]
    )

    calculated_final_rank = electre_ii(
        dataset,
        weight,
        scales,
        c_hat_sup,
        c_hat_inf,
        d_hat_sup,
        d_hat_inf,
        concordance_matrix,
        discordance_matrix,
    )
    assert_frame_equal(calculated_final_rank, final_rank, check_dtype=False)
