import warnings

from mcda.core.sorting import rank_values, sort_elements_by_values


def test_rank_values():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    values = [0, -10, 12, 5.5, -0.8]
    res = rank_values(values)
    pred_res = [2, 4, 0, 1, 3]
    for r, p in zip(res, pred_res):
        assert r == p
    res = rank_values(values, True)
    pred_res = [2, 0, 4, 3, 1]
    for r, p in zip(res, pred_res):
        assert r == p


def test_sort_elements_by_values():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    elements = ["a", "b", "c", "d", "e"]
    values = [0, -10, 12, 5.5, -0.8]
    sort_elements_by_values(values, elements, values)
    pred_res = ["c", "d", "a", "e", "b"]
    for r, p in zip(elements, pred_res):
        assert r == p
    pred_res = [12, 5.5, 0, -0.8, -10]
    for v, p in zip(values, pred_res):
        assert v == p
