import math
import unittest

from mcda.core.set_functions import (
    interaction_index_capacity,
    interaction_index_mobius,
    inverse_mobius_transform,
    is_additive,
    is_capacity,
    is_cardinality_based,
    is_game,
    is_k_additive,
    is_mobius_capacity,
    is_normal_capacity,
    is_normal_capacity_mobius,
    is_powerset_function,
    mobius_transform,
    mobius_transform_fast,
    mobius_transform_numpy,
    shapley_capacity,
    shapley_mobius,
    uniform_capacity,
)


def test_is_powerset_function():
    assert is_powerset_function([0])
    assert is_powerset_function([0, 1, 2, 3])
    assert not is_powerset_function([0, 1, 2, 3, 4])


def test_is_game():
    assert not is_game([0, 1, 2, 3, 4])
    assert is_game([0, 1, 2, 3])
    assert not is_game([1, 1, 2, 3])


class SetsFunctionsTestCase(unittest.TestCase):
    def setUp(self):
        # Value taken from R kappalab package (7 digits accuracy)
        self.capacity = [
            0.0,
            0.07692307692307693,
            0.15384615384615385,
            0.38461538461538464,
            0.23076923076923078,
            0.46153846153846156,
            0.6153846153846154,
            0.8461538461538461,
            0.3076923076923077,
            0.5384615384615384,
            0.6923076923076923,
            0.9230769230769231,
            0.7692307692307693,
            1.0,
            1,
            1,
        ]
        self.mobius = [
            0,
            0.07692308,
            0.15384615,
            0.15384615,
            0.23076923,
            0.15384615,
            0.23076923,
            -0.15384615,
            0.30769231,
            0.15384615,
            0.23076923,
            -0.15384615,
            0.23076923,
            -0.15384615,
            -0.38461538,
            -0.07692308,
        ]
        self.shapley = [0.1346154, 0.2115385, 0.2884615, 0.3653846]
        self.interaction_indexes = [
            [float("nan"), -0.02564103, -0.02564103, -0.02564103],
            [-0.02564103, float("nan"), -0.06410256, -0.06410256],
            [-0.02564103, -0.06410256, float("nan"), -0.06410256],
            [-0.02564103, -0.06410256, -0.06410256, float("nan")],
        ]

    def test_is_capacity(self):
        self.assertFalse(is_capacity([1, 1, 2, 3]))
        self.assertFalse(is_capacity([0, 0.1, 1, 0.6, 0.2, 0.25, 0.5, 2]))
        self.assertTrue(is_capacity(self.capacity))

    def test_is_normal_capacity(self):
        self.assertFalse(
            is_normal_capacity([0, 0.1, 1, 0.6, 0.2, 0.25, 0.5, 2])
        )
        self.assertFalse(is_normal_capacity([0, 0.5, 0.2, 2]))
        self.assertTrue(is_normal_capacity(self.capacity))

    def test_is_additive(self):
        self.assertFalse(is_additive(self.capacity))
        self.assertTrue(is_additive([0, 1, 1.5, 2.5]))

    def test_uniform_capacity(self):
        for n in range(1, 5):
            c = uniform_capacity(n)
            self.assertEqual(len(c), 2 ** n)
            self.assertTrue(is_normal_capacity(c))
            self.assertTrue(is_additive(c))

    def test_is_k_additive(self):
        self.assertFalse(is_k_additive([0, 1, 2, 2], 1))
        self.assertFalse(is_k_additive([0, 0, 0, 0], 1))
        self.assertTrue(is_k_additive([0, 1, 2, 0], 1))
        self.assertTrue(is_k_additive([0, 1, 2, 3, 0, 0, 0, 0], 2))
        self.assertFalse(is_k_additive([0, 1, 2, 3, 0, 0, 0, 0], 1))

    def test_is_cardinality_based(self):
        self.assertFalse(is_cardinality_based([0, 1, 2]))
        self.assertTrue(is_cardinality_based([1, 2, 2, 1, 2, 1, 1]))

    def test_mobius_transform(self):
        m = mobius_transform(self.capacity)
        for m1, m2 in zip(m, self.mobius):
            self.assertAlmostEqual(m1, m2)  # Has 7-digits accuracy as R

    def test_mobius_transform_fast(self):
        m = mobius_transform_fast(self.capacity)
        for m1, m2 in zip(m, self.mobius):
            self.assertAlmostEqual(m1, m2)  # Has 7-digits accuracy as R

    def test_mobius_transform_numpy(self):
        m = mobius_transform_numpy(self.capacity)
        for m1, m2 in zip(m, self.mobius):
            self.assertAlmostEqual(m1, m2)  # Has 7-digits accuracy as R

    def test_inverse_mobius_transform(self):
        c = inverse_mobius_transform(self.mobius)
        for c1, c2 in zip(c, self.capacity):
            self.assertAlmostEqual(c1, c2)  # Has 7-digits accuracy as R

    def test_is_mobius_capacity(self):
        self.assertFalse(is_mobius_capacity([1, 2]))
        mobius = mobius_transform(self.capacity)
        self.assertTrue(is_mobius_capacity(mobius))
        self.assertTrue(is_mobius_capacity([0, 1 / 3, 1 / 3, 1 / 3]))
        self.assertFalse(is_mobius_capacity([0, -1, 0, 0]))

    def test_is_normal_capacity_mobius(self):
        self.assertFalse(is_normal_capacity_mobius([0, -1, 0, 0]))
        self.assertTrue(
            is_normal_capacity_mobius(mobius_transform(self.capacity))
        )
        self.assertTrue(is_normal_capacity_mobius([0, 1 / 3, 1 / 3, 1 / 3]))
        self.assertFalse(is_normal_capacity_mobius([0, 0.25, 0.25, 0.25]))

    def test_shapley_capacity(self):
        s = shapley_capacity(self.capacity)
        for s1, s2 in zip(s, self.shapley):
            self.assertAlmostEqual(s1, s2)

    def test_shapley_mobius(self):
        s = shapley_mobius(self.mobius)
        for s1, s2 in zip(s, self.shapley):
            self.assertAlmostEqual(s1, s2)

    def test_interaction_index_capacity(self):
        res = interaction_index_capacity(self.capacity)
        for r1, r2 in zip(res, self.interaction_indexes):
            for rr1, rr2 in zip(r1, r2):
                if math.isnan(rr1):
                    self.assertTrue(math.isnan(rr2))
                    continue
                self.assertAlmostEqual(rr1, rr2)

    def test_interaction_index_mobius(self):
        res = interaction_index_mobius(self.mobius)
        for r1, r2 in zip(res, self.interaction_indexes):
            for rr1, rr2 in zip(r1, r2):
                if math.isnan(rr1):
                    self.assertTrue(math.isnan(rr2))
                    continue
                self.assertAlmostEqual(rr1, rr2)
